import { html } from 'lit-element';
import {
  MasterPage,
  gridStyles,
  stylesPages,
  buttonsStyles,
} from '../../elements/utils/master-page.js';
import styles from './accounts-page-styles.js';
import '../../elements/components/cells-card-panel/cells-card-panel.js';
import '@bbva-web-components/bbva-web-form-text/bbva-web-form-text.js';
import '@bbva-web-components/bbva-web-form-select/bbva-web-form-select.js';
import '@bbva-web-components/bbva-form-textarea/bbva-form-textarea.js';
import '@cells-components/cells-icon/cells-icon.js';
import '../../elements/components/cells-data-table/cells-data-table.js';
import '../../elements/components/cells-dropdown-menu/cells-dropdown-menu.js';
import { columsnAccount } from '../../elements/utils/columns-config.js';
import '../../elements/services/accounts-dm/accounts-dm.js';

class AccountsPage extends MasterPage {
  static get is() {
    return 'accounts-page';
  }

  static get styles() {
    return [stylesPages, gridStyles, buttonsStyles, styles];
  }

  static get properties() {
    return {
      accounts: Array,
      accountSelected: Object
    };
  }

  get accountsDm() {
    return this.element('accounts-dm');
  }

  constructor() {
    super();
    this.accounts = [];
  }

  get titleForm() {
    return this.accountSelected ? 'Editar cuenta' : 'Crear cuenta';
  }

  listAccounts({ detail: accounts }) {
    console.log('rendedirzar list accounts', accounts);
    let temp = [ ... accounts ];
    this.accounts = [];
    this.accounts = temp;
    this.requestUpdate();
  }

  onCompleteRegister({detail}) {
    console.log('onCompleteRegister', detail);
    let type = 'success';
    let icon = 'coronita:correct';
    let msg = this.extract(this.appProps, 'messages.accounts.createSuccess', '');
    if (!(detail.request.ok && detail.request.status === 200)) {
      msg = this.extract(this.appProps, 'messages.accounts.createError', '');
      type = 'error';
      icon = 'coronita:error';
      msg = msg.replace('{1}', 'la cuenta ya existe');
    } else {
      if (this.accountSelected && this.accountSelected.id) {
        msg = msg.replace('{1}', 'actualizada');
      } else {
        msg = msg.replace('{1}', 'registrada');
      }
      this.resetForm();
      this.accountsDm.listAccounts();
    }
    msg = msg.replace('{0}', `<b>${detail.payload.number}</b>`);
    this.messageToast({
      show: true,
      message: msg,
      type: type,
      icon: icon
    });
  }

  valideteFormAccount() {
    const formInputs = this.elementsAll('.required');
    let countsErrors = 0;
    formInputs.forEach(element => {
      if (this.isEmpty(element.value)) {
        element.classList.add('invalid');
        countsErrors++;
      } else {
        element.classList.remove('invalid');
      }
    });
    return countsErrors === 0;
  }

  clearInvalid() {
    const formInputs = this.elementsAll('.formInput');
    formInputs.forEach(element => {
      element.value = '';
      element.classList.remove('invalid');
    });
  }

  resetForm() {
    this.accountSelected = null;
    this.clearInvalid();
    this.element('#accounts-table').resetSelections();
  }

  async createAccount() {
    if (this.valideteFormAccount()) {
      let account = {};
      const formInputs = this.elementsAll('.formInput');
      formInputs.forEach(element => {
        account[element.name] = element.value;
      });
      console.log('Payload-createAccount{account}', account);
      await this.accountsDm.createAccount(account, this.accountSelected);
    }
  }

  onPageEnter() {
    this.searchAccounts(true);
  }

  getParams() {
    const filterInputs = this.elementsAll('.filter-input');
    let params = {};
    filterInputs.forEach(inputFilter => {
      if (!this.isEmpty(inputFilter.value)) {
        params[inputFilter.name] = inputFilter.value;
      }
    });
    return params;
  }

  searchAccounts(isFirstSearch) {
    this.resetForm();
    const params = this.getParams();
    if (isFirstSearch) {
      params['status.id'] = 'ACTIVE';
    }
    console.log('paramsSearch', params);
    this.accountsDm.listAccounts(params);
  }

  onPageLeave() {}

  exportReport() {
    const exportData = [];
    console.log('export', this.accounts);
    this.accounts.forEach((item) => {
      let row = `${item.number}`;
      row += `|${item.name}`;
      row += `|${item.shortName}`;
      row += `|${item.currency}`;
      row += `|${item.swift}`;
      row += `|${this.extract(item, 'bank.id')}`;
      row += `|${this.extract(item, 'status.id')}`;
      row += `|${this.extract(item, 'creationDate')}`;
      row += `|${this.extract(item, 'bank.location.country')}`;
      row += `|${this.extract(item, 'bank.location.city')}`;
      row += `|${this.extract(item, 'bank.location.addressName')}`;
      row += `|${this.extract(item, 'bank.location.branch.name')} - ${this.extract(item, 'bank.location.branch.id')}`;
      exportData.push(row);
    });
    let headersReport = 'N° cuenta';
    headersReport = headersReport + '|Nombre de la cuenta';
    headersReport = headersReport + '|Nombre abreviado';
    headersReport = headersReport + '|Moneda';
    headersReport = headersReport + '|Código swift';
    headersReport = headersReport + '|BIC';
    headersReport = headersReport + '|Estado';
    headersReport = headersReport + '|Fecha creación';
    headersReport = headersReport + '|Pais';
    headersReport = headersReport + '|Ciudad';
    headersReport = headersReport + '|Dirección';
    headersReport = headersReport + '|Oficina';
    const data = [ headersReport.split('|') ];
    exportData.forEach((rowData) => {
      data.push(rowData.split('|'));
    });
    this.exportToExcel(data);
  }

  exportToExcel(data) {
    let csvContent = '';
    data.forEach((infoArray, index) => {
      const dataString = infoArray.join(';');
      csvContent += index < data.length ? `${dataString}\n` : dataString;
    });
    const universalBOM = '\uFEFF';
    const BOM = new window.Uint8Array([0xef, 0xbb, 0xbf]);
    const content = csvContent;
    const fileName = `accounts_report_${new Date().getTime()}.CSV`;
    let mimeType = 'text/csv;encoding:utf-8';
    const a = document.createElement('a');
    mimeType = mimeType || 'application/octet-stream';
    if (navigator.msSaveBlob) {
      navigator.msSaveBlob(
        new Blob([BOM, content], {
          type: mimeType,
        }),
        fileName
      );
    } else if (URL && 'download' in a) {
      a.href = URL.createObjectURL(
        new Blob([BOM, content], {
          type: mimeType,
        })
      );
      a.setAttribute('download', fileName);
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    } else {
      location.href = `data:application/octet-stream,${encodeURIComponent(
        universalBOM + content
      )}`;
    }
  }

  onAccountSelected({detail}) {
    console.log('onAccountSelected', detail);
    this.accountSelected = detail;
    this.clearInvalid();
    const inputs = this.elementsAll('.formInput');
    inputs.forEach(input => {
      input.value = this.extract(this.accountSelected, input.dataset.key);
    });
  }

  onAccountDeSelected({detail}) {
    console.log('onAccountDeSelected', detail);
    this.resetForm();
  }

  searchEnterAccount(evt) {
    const keycode = (evt.keyCode ? evt.keyCode : evt.which);
    if (keycode === 13) {
      this.searchAccounts();
    }
  }

  render() {
    return this.content(html`
      <h2 class="title-page">Administración de cuentas</h2>
      <div class="row">
        <div
          class="col-12	col-sm-7 col-md-7 col-lg-7 col-xl-7	col-xxl-7 col-section"
        >
          <cells-card-panel headerTitle="Consulta de cuentas">
            <div slot="body" class="panel-content">
              <div class="filter-panel">
                <div class="filter-panel-numcuenta">
                  <bbva-web-form-text
                    class="filter-input"
                    name="number"
                    label="Número de cuenta"
                    @keyup="${this.searchEnterAccount}"
                  ></bbva-web-form-text>
                </div>
                <div class="filter-panel-moneda">
                  <bbva-web-form-select 
                      class="filter-input" 
                      name="currency" 
                      label="Moneda"
                      @change="${() => this.searchAccounts() }">
                    <bbva-web-form-option selected value="">TODOS</bbva-web-form-option>
                    <bbva-web-form-option value="PEN">PEN</bbva-web-form-option>
                    <bbva-web-form-option value="USD">USD</bbva-web-form-option>
                    <bbva-web-form-option value="EUR">EUR</bbva-web-form-option>
                  </bbva-web-form-select>
                </div>
                <div class="filter-panel-estado">
                  <bbva-web-form-select 
                        class="filter-input" 
                        name="status.id" 
                        label="Estado"
                        @change="${() => this.searchAccounts() }" >
                    <bbva-web-form-option value="">TODOS</bbva-web-form-option>
                    <bbva-web-form-option selected value="ACTIVE">Activo</bbva-web-form-option>
                    <bbva-web-form-option value="INACTIVE">Inactivo</bbva-web-form-option>
                  </bbva-web-form-select>
                </div>
                <div class="filter-panel-buttons">
                  <button @click="${() => this.searchAccounts() }" class="button button-blue btn-filter">
                    <cells-icon icon="coronita:search"></cells-icon>
                  </button>
                  <button @click="${this.exportReport}" class="button button-aquadark btn-filter btn-export">
                    <cells-icon icon="coronita:download"></cells-icon>
                  </button>
                </div>
              </div>

              <div class="list-panel">
                <cells-data-table
                  cls="white"
                  id="accounts-table"
                  pageSize="7"
                  modePagination="memory"
                  .showPanelPaginationTop="${true}"
                  .showPanelPaginationBottom="${false}"
                  .items="${this.accounts}"
                  .columnsConfig="${columsnAccount}"
                  titleNoResult="${this.extract(this.appProps, 'messages.noResult.title')}"
                  msgNoResult="${this.extract(this.appProps, 'messages.noResult.msg')}"
                  @on-selected-row="${this.onAccountSelected}"
                  @on-deselected-row="${this.onAccountDeSelected}"
                ></cells-data-table>
              </div>
            </div>
          </cells-card-panel>
        </div>
        <div
          class="col-12	col-sm-5 col-md-5 col-lg-5 col-xl-5	col-xxl-5 col-section"
        >
          <cells-card-panel headerTitle="${this.titleForm}">
            <div slot="body" class="form-account">
              <div class="row">
                <!--Numero cuenta - Moneda -->
                <div
                  class="col-12	col-sm-8 col-md-8 col-lg-8 col-xl-8	col-xxl-8 col-section"
                >
                  <bbva-web-form-text 
                    class="formInput required" 
                    name="number"
                    data-key="number"
                    label="Número de cuenta"
                  ></bbva-web-form-text>
                </div>
                <div
                  class="col-12	col-sm-4 col-md-4 col-lg-4 col-xl-4	col-xxl-4 col-section"
                >
                  <bbva-web-form-select data-key="currency" label="Moneda" class="formInput required" name="currency">
                    <bbva-web-form-option value="PEN">PEN</bbva-web-form-option>
                    <bbva-web-form-option value="USD">USD</bbva-web-form-option>
                    <bbva-web-form-option value="EUR">EUR</bbva-web-form-option>
                  </bbva-web-form-select>
                </div>
                <!-- / -->

                <!-- Nombre cuenta - abreviatura -->
                <div
                  class="col-12	col-sm-8 col-md-8 col-lg-8 col-xl-8	col-xxl-8 col-section"
                >
                  <bbva-web-form-text
                    data-key="name"
                    label="Nombre de la cuenta"
                    class="formInput required" name="name"
                  ></bbva-web-form-text>
                </div>
                <div
                  class="col-12	col-sm-4 col-md-4 col-lg-4 col-xl-4	col-xxl-4 col-section"
                >
                  <bbva-web-form-text data-key="shortName" label="Abreviatura" class="formInput required" name="shortName" ></bbva-web-form-text>
                </div>
                <!-- / -->

                <!-- Numero BIC - cta SWIFT - Estado -->
                <div
                  class="col-12	col-sm-4 col-md-4 col-lg-4 col-xl-4	col-xxl-4 col-section"
                >
                  <bbva-web-form-text data-key="bank.id" label="Número BIC" class="formInput required" name="bic" ></bbva-web-form-text>
                </div>

                <div
                  class="col-12	col-sm-4 col-md-4 col-lg-4 col-xl-4	col-xxl-4 col-section"
                >
                  <bbva-web-form-text label="Cuenta SWIFT" data-key="swift" class="formInput required" name="swift" ></bbva-web-form-text>
                </div>

                <div
                  class="col-12	col-sm-4 col-md-4 col-lg-4 col-xl-4	col-xxl-4 col-section"
                >
                  <bbva-web-form-select label="Estado" class="formInput required" data-key="status.id" name="statusId">
                    <bbva-web-form-option value="ACTIVE"
                      >Activo</bbva-web-form-option
                    >
                    <bbva-web-form-option value="INACTIVE"
                      >Inactivo</bbva-web-form-option
                    >
                  </bbva-web-form-select>
                </div>
                <!-- / -->

                <!-- Direccion banco -->
                <div
                  class="col-12	col-sm-12 col-md-12 col-lg-12 col-xl-12	col-xxl-12 col-section"
                >
                  <bbva-web-form-text
                    class="formInput" 
                    name="addressName"
                    data-key="bank.location.addressName"
                    label="Dirección del banco"
                  ></bbva-web-form-text>
                </div>
                <!-- / -->

                <!-- Localidad - Pais -->
                <div
                  class="col-12	col-sm-6 col-md-6 col-lg-6 col-xl-6	col-xxl-6 col-section"
                >
                  <bbva-web-form-text
                    class="formInput" 
                    name="city"
                    data-key="bank.location.city"
                    label="Localidad del banco"
                  ></bbva-web-form-text>
                </div>
                <div
                  class="col-12	col-sm-6 col-md-6 col-lg-6 col-xl-6	col-xxl-6 col-section"
                >
                  <bbva-web-form-text data-key="bank.location.country" label="País" class="formInput required" name="country" ></bbva-web-form-text>
                </div>
                <!-- / -->

                <!-- Oficina - codigo -->
                <div
                  class="col-12	col-sm-8 col-md-8 col-lg-8 col-xl-8	col-xxl-8 col-section"
                >
                  <bbva-web-form-text
                    class="formInput required" 
                    data-key="bank.location.branch.name"
                    name="branchName" 
                    label="Oficina responsable"
                  ></bbva-web-form-text>
                </div>
                <div
                  class="col-12	col-sm-4 col-md-4 col-lg-4 col-xl-4	col-xxl-4 col-section"
                >
                  <bbva-web-form-text label="Código" data-key="bank.location.branch.id" class="formInput required" name="branchCode" ></bbva-web-form-text>
                </div>
                <!-- / -->

                <!-- Observaciones -->
                <div
                  class="col-12	col-sm-12 col-md-12 col-lg-12 col-xl-12	col-xxl-12 col-section"
                >
                  <bbva-form-textarea
                    label="Observaciones"
                    maxlength="200"
                    class="formInput" 
                    name="comment"
                    data-key="comment" 
                  ></bbva-form-textarea>
                </div>
                <!-- / -->

                <!-- Botones -->
                <div
                  class="col-12	col-sm-12 col-md-12 col-lg-12 col-xl-12	col-xxl-12 col-section buttons-form"
                >
                  <button class="button button-gray lg" @click="${this.resetForm}">Cancelar</button>
                  <button class="button button-blue lg" @click="${this.createAccount}" >${this.accountSelected ? 'Actualizar' : 'Registrar'}</button>
                </div>
                <!-- -->
              </div>
            </div>
          </cells-card-panel>
        </div>
      </div>

      <!-- AccountsDm-->
      <accounts-dm
      @list-accounts="${this.listAccounts}"
      @on-complete-create-account="${this.onCompleteRegister}"
      ></accounts-dm>
    `);
  }
}
customElements.define(AccountsPage.is, AccountsPage);
