/* eslint-disable no-unused-vars */
import { css, unsafeCSS } from 'lit-element';
import * as foundations from '@bbva-web-components/bbva-foundations-styles';

export default css`:host {
  display: block;
  box-sizing: border-box;
}

:host([hidden]),
[hidden] {
  display: none !important;
}

.panel-content {
  min-height: calc(100vh - 250px);
}
.panel-content .filter-panel {
  display: flex;
  align-items: center;
  justify-items: start;
}
.panel-content .filter-panel * {
  margin-right: 5px;
}
.panel-content .filter-panel-numcuenta {
  width: calc(100% - 356px);
}
.panel-content .filter-panel-moneda {
  width: 150px;
}
.panel-content .filter-panel-estado {
  width: 150px;
}
.panel-content .filter-panel-buttons {
  width: 100px;
  margin-right: 0;
  display: flex;
  align-items: center;
  justify-content: flex-start;
}
.panel-content .filter-panel-buttons .btn-export {
  margin-right: 0;
}
.panel-content .filter-panel-buttons .btn-filter {
  padding-left: 0px;
  padding-right: 0px;
  width: 48px;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 48px !important;
}
.panel-content .filter-panel-buttons .btn-filter cells-icon {
  margin: 1px 0px 0px 4px;
  width: 27px;
  height: 27px;
}
.panel-content .list-panel {
  margin-top: 10px;
}

.form-account {
  min-height: calc(100vh - 250px);
  padding-left: 5px;
  padding-right: 5px;
  overflow: hidden;
}
.form-account .col-section {
  padding: 1px 5px;
}
.form-account .buttons-form {
  display: flex;
  align-items: center;
  justify-content: space-between;
}

.invalid {
  --bbva-web-form-field-border-color: #E77D8E;
  --bbva-web-form-field-focused-border-color: #E77D8E;
  --bbva-web-form-field-bg-color: #F4C3CA;
  --bbva-web-form-select-bg-color: #F4C3CA;
  --bbva-web-form-select-border-color: #E77D8E;
}
`;