/* eslint-disable no-unused-vars */
import { css, unsafeCSS } from 'lit-element';
import * as foundations from '@bbva-web-components/bbva-foundations-styles';

export default css`:host {
  display: block;
  box-sizing: border-box;
  --microilustration-w:120px;
  --microilustration-h:110px;
}

:host([hidden]),
[hidden] {
  display: none !important;
}

.header-title-home {
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid #e1e1e1;
  padding-bottom: 10px;
}
.header-title-home h2 {
  font-size: 1.3em;
  margin: 0;
  padding: 0;
  padding-top: 25px;
  width: calc(100% - 390px);
}
.header-title-home .ejecutions-sections {
  display: flex;
  align-items: center;
}
.header-title-home .ejecutions-sections bbva-web-form-date {
  width: 240px;
  margin-right: 5px;
}
.header-title-home .ejecutions-sections bbva-web-button-default {
  --_bg-color: #006C6C;
  --_ripple-color: #028484;
  width: 150px;
}

.container-information {
  margin-top: 15px;
}
.container-information .panel-content {
  background-color: #F4F4F4;
}
.container-information .result-process {
  display: flex;
  align-items: center;
  overflow: hidden;
}
.container-information .result-process .microilustration {
  width: var(--microilustration-w);
  height: var(--microilustration-h);
}
.container-information .result-process .account-mi {
  padding-bottom: 8px;
  margin-top: -8px;
}
.container-information .result-process .text-info-result {
  width: calc(100% - var(--microilustration-w));
  display: flex;
  flex-direction: column;
}
.container-information .result-process .text-info-result .item-text {
  line-height: 1.3em;
  font-size: 0.9em;
  color: #2B2F3A;
  display: flex;
  align-items: center;
}
.container-information .result-process .text-info-result .item-text cells-icon {
  width: 40px;
  margin: 0 8px 0 5px;
  color: #006C6C;
}
.container-information .result-process .text-info-result .item-text strong {
  font-size: 0.9em;
}
.container-information .result-process .text-info-result .right-text-result {
  padding-left: 20px;
}
.container-information .result-process .text-info-result .right-text-result a {
  color: #2B2F3A;
  font-weight: bold;
}
.container-information .result-process .text-info-result .item-text:first-child {
  margin-bottom: 10px;
}

.title-eecc-bcr {
  margin-top: 15px;
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.title-eecc-bcr cells-icon {
  cursor: pointer;
}

.container-eecc-group {
  margin-top: 15px;
}

.separator-block-eecc {
  margin-top: 10px;
}
`;