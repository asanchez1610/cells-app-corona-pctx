import { html } from 'lit-element';
import '@cells-components/cells-template-paper-drawer-panel';
import {
  MasterPage,
  gridStyles,
  stylesPages,
} from '../../elements/utils/master-page.js';
import styles from './home-page-styles.js';
import '@bbva-web-components/bbva-web-form-date/bbva-web-form-date';
import '@bbva-web-components/bbva-button-default/bbva-button-default';
import '../../elements/components/cells-dropdown-menu/cells-dropdown-menu';
import '../../elements/components/cells-card-panel/cells-card-panel';
import {
  programyouraccountservicedark,
  accountsmigrationdark,
} from '@bbva-web-components/bbva-foundations-microillustrations/bbva-foundations-microillustrations';
import '../../elements/components/cells-eecc-detail/cells-eecc-detail';

const options = [
  {
    title: 'Proceso NONE',
    name: 'send',
    icon: 'coronita:navigation',
    color: '#006C6C',
  },
  {
    title: 'Proceso SWIFT',
    name: 'delete',
    icon: 'coronita:frequency',
    color: '#043263',
  }
];


class HomePage extends MasterPage {
  static get is() {
    return 'home-page';
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
    console.log('this.appProps in HomePage', this.appProps);
  }

  firstUpdated() {
    this.element('#date-process').value = this.momentJs((new Date())).format('YYYY-MM-DD');
  }

  static get styles() {
    return [stylesPages, gridStyles, styles];
  }

  openMenuEjecutar(e) {
    e.stopPropagation();
    let dropdownMenu =  this.element('cells-dropdown-menu');
    const { x, y } = this.getPositionElement(e.target);
    dropdownMenu.show(x - 28, y + e.target.offsetHeight);
  }

  render() {
    return this.content(html`
      <div class="header-title-home">
        <h2>Información del proceso automático de conciliaciones</h2>
        <div>
          <div class="ejecutions-sections">
            <bbva-web-form-date id="date-process" label="Fecha de proceso"></bbva-web-form-date>
            <bbva-web-button-default @click="${this.openMenuEjecutar}" id="run-process" >Ejecutar</bbva-web-button-default>
            <cells-dropdown-menu .options="${options}"></cells-dropdown-menu>
          </div>
        </div>
      </div>
      <main class="container-information">
        <div class="row">
          <div
            class="col-12	col-sm-6 col-md-6 col-lg-6 col-xl-6	col-xxl-6 col-section"
          >
            <cells-card-panel class="panel-content">
              <div slot="body">
                <div class="result-process">
                  <div class="microilustration">
                    ${programyouraccountservicedark}
                  </div>
                  <div class="text-info-result">
                    <div class="item-text">
                      <cells-icon icon="coronita:correct"></cells-icon>
                      <span
                        >Verificacion de la existencia y consistencia de los
                        archivos para el procesamiento
                        <strong>ALL</strong>.</span
                      >
                    </div>
                    <div class="item-text">
                      <cells-icon icon="coronita:correct"></cells-icon>
                      <span
                        >Veriificacion de la ejecución del proceso
                        <strong>ALL</strong> de las operaciones contables.</span
                      >
                    </div>
                  </div>
                </div>
              </div>
            </cells-card-panel>
          </div>

          <div
            class="col-12	col-sm-6 col-md-6 col-lg-6 col-xl-6	col-xxl-6 col-section"
          >
            <cells-card-panel class="panel-content">
              <div slot="body">
                <div class="result-process">
                  <div class="microilustration account-mi">
                    ${accountsmigrationdark}
                  </div>
                  <div class="text-info-result">
                    <div class="item-text right-text-result">
                      <span
                        >Total de cuentas con balance diferente:
                        <a href="#">0</a></span
                      >
                    </div>
                    <div class="item-text right-text-result">
                      <span
                        >Total de estados de cuentas con GAP’s:&nbsp;
                        <a href="#">0</a></span
                      >
                    </div>
                  </div>
                </div>
              </div>
            </cells-card-panel>
          </div>
        </div>
      </main>
      <h2 class="title-page title-eecc-bcr">
        Estados de cuenta del BCR
        <cells-icon icon="coronita:print"></cells-icon>
      </h2>
      <main class="container-eecc-group">
        <div class="row">
          <div
            class="col-12	col-sm-6 col-md-6 col-lg-6 col-xl-6	col-xxl-6 col-section"
          >
            <cells-eecc-detail
              panelTitle="11120101 PEN - BANCO CENTRAL DE RESERVA M. N."
            ></cells-eecc-detail>
          </div>
          <div
            class="col-12	col-sm-6 col-md-6 col-lg-6 col-xl-6	col-xxl-6 col-section"
          >
            <cells-eecc-detail
              panelTitle="11220101 USD - BCR DOLARES"
            ></cells-eecc-detail>
          </div>
        </div>

        <div class="row separator-block-eecc">
          <div
            class="col-12	col-sm-6 col-md-6 col-lg-6 col-xl-6	col-xxl-6 col-section"
          >
            <cells-eecc-detail
              panelTitle="11120401 PEN - B.C.R. OVERNIGHT SOLES"
            ></cells-eecc-detail>
          </div>
          <div
            class="col-12	col-sm-6 col-md-6 col-lg-6 col-xl-6	col-xxl-6 col-section"
          >
            <cells-eecc-detail
              panelTitle="11220401 USD - BCR - DEPOSITOS OVERNIGHT"
            ></cells-eecc-detail>
          </div>
        </div>
      </main>
    `);
  }

  onPageEnter() {}

  onPageLeave() {}
}

window.customElements.define(HomePage.is, HomePage);
