(function() {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'home': '/',
      'accounts': '/accounts'
    }
  });
}());
