// Import here your LitElement initial components (critical / startup)

import '@bbva-web-components/bbva-core-scoping-ambients-shim';
import '@webcomponents/shadycss/entrypoints/custom-style-interface.js';
import '@bbva-web-components/bbva-foundations-theme/bbva-foundations-theme.js';
import '@cells-components/coronita-icons';
import '@bbva-web-components/bbva-core-moment-import/lib/bbva-core-moment-import.min.js';
import '../elements/components/cells-header-menu/cells-header-menu.js';
import '../elements/components/cells-commons-access-control/cells-commons-access-control.js';
import '../elements/components/cells-commons-dp/cells-commons-dp.js';
import '@bbva-web-components/bbva-help-modal/bbva-help-modal';
import '../elements/components/cells-mask-loading/cells-mask-loading.js';
import '@bbva-web-components/bbva-notification-toast/bbva-notification-toast.js';