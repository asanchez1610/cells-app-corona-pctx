const props = require('./props.js');
let props_base = { ...props.props_base() };

props_base.app_properties.host = 'http://www.soft-dk.com:1337';

props_base.app_properties.securityConf = {
  urlGrantingTicket: 'https://int-corona-pe.work-02.nextgen.igrupobbva/TechArchitecture/pe/grantingTicket/V02',
  aapId: '13000105',
  urlApplication: 'https://ei-community.grupobbva.com/'
};

props_base.app_properties.services= {
  host: 'https://int-corona-pe.work-02.nextgen.igrupobbva',
  paths: {
    issueTracker: 'product-issue-tracker/v0/issues'
  }
};

const appConfig = { ...props_base };

module.exports = appConfig;