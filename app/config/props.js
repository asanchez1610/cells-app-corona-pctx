const msg = require('./messages.js');
let messages = { ...msg.messages() };

function props_base() {
  return {
    /**
     * Properties used to control settings of Cells Bridge and the build process
     */
    cells_properties: {
      enableLitElement: true,
      onlyLitElements: true,
      transpile: true,
      transpileExclude: ['webcomponentsjs', 'moment', 'd3', 'bgadp*'],

      debug: true,
      logs: false,

      /**
       * Relative path to folder that contains dynamic pages (.json files)
       */
      templatesPath: './dynamicPages/',
      /**
       * Relative path to folder that contains static pages (.js files)
       */
      pagesPath: './pages/',
      prplLevel: 1,
      initialBundle: ['home'],

      /* Internationalization options */
      locales: {
        languages: ['es-ES', 'en-US', 'es'],
        intlInputFileNames: ['locales'],
        intlFileName: 'locales',
      },
      messages: messages
    },

    /**
     * These properties are especific to your application.
     * Here you can use your own properties, so it is an
     * open set of properties that you can use at your
     * convinience.
     * These variables will be available in your
     * application ins the window.AppConfig object
     */
    app_properties: {
      mock: true,
      titleApp: 'Corona 2.0',
      imgFProfile: 'resources/images/avatar-famele.svg',
      imgMProfile: 'resources/images/avatar-male.svg',
      logo: 'resources/images/logo_bbva_blanco.svg',
      logoMenu: 'resources/images/logo_bbva_blanco.svg',
      viewMode: 'vertical',
      authenticationMode: 'employee',
      userIntegrated: false,
      options_menu: [
        {
          name: 'Inicio',
          icon: 'coronita:home',
          page: '#!/',
        },
        {
          name: 'Procesar conciliaciones',
          icon: 'coronita:tasks',
          page: '#!/',
        },
        {
          name: 'Gastos OUR',
          icon: 'coronita:currencyexchange',
          page: '#!/',
        },
        {
          name: 'Titulaciones',
          icon: 'coronita:billspayments',
          page: '#!/',
        },
        {
          name: 'Administración de cuentas',
          icon: 'coronita:account',
          page: '#!/accounts',
        },
        {
          name: 'Estados de cuenta',
          icon: 'coronita:balance',
          page: '#!/',
        },
        {
          name: 'Reportes',
          icon: 'coronita:consult',
          page: '#!/',
        }
      ],
      paths: {
        accounts: {
          list: 'accounts',
          create: 'accounts'
        }
      }
    },
  };
}

module.exports = {
  props_base: props_base,
};
