const props = require('./props.js');
let props_base = { ...props.props_base() };

props_base.app_properties.host = 'http://www.soft-dk.com:1337';

props_base.app_properties.securityConf = {
  urlGrantingTicket: 'https://cal-glomo.bbva.pe/SRVS_A02/TechArchitecture/pe/grantingTicket/V02',
  aapId: '13000027',
  ivUser: 'pe.P024097',
  ivTicket: 'jCGFlIAHnll64aJky8XzGSWE4Rkrt43MvWybRnoO9hda5ieGPZMlmVKP4RqbYgVG'
};

props_base.app_properties.services= {
  host: 'https://cal-glomo.bbva.pe/SRVS_A02',
  paths: {
    issueTracker: 'product-issue-tracker/v0/issues'
  }
};

const appConfig = { ...props_base };

module.exports = appConfig;