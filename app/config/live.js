const props = require('./props.js');

let props_base = { ...props.props_base() };

props_base.app_properties.host = 'http://www.soft-dk.com:1337';

props_base.app_properties.securityConf = {
  urlGrantingTicket: 'https://aso.grupobbva.com.pe/PFCC_PR_01/TechArchitecture/pe/grantingTicket/V02',
  aapId: '13000027',
  ivUser: 'xp63127',
  ivTicket: 'Z+eukfn8QZWsxWCab1zFBSFGcsWYglXzi7pjPiDO11iXlgm4YNagQG6aZXHBbmbG'
};

props_base.app_properties.services= {
  host: 'https://aso.grupobbva.com.pe/PFCC_PR_01',
  paths: {
    issueTracker: 'product-issue-tracker/v0/issues'
  }
};

const appConfig = { ...props_base };

module.exports = appConfig;