function messages() {
    return {
        noResult: {
            title: 'No se encontraron resultados para mostrar',
            msg: 'Es posible que no existan registros para mostrar ó no se presentan coincidencias con los filtros de busqueda informados.'
        },
        accounts: {
            createSuccess: 'La cuenta {0} ha sido {1} de forma exitosa.',
            createError: 'Error al registrar la cuenta {0}, {1}.',
        }
    };
}

module.exports = {
    messages: messages
  };
  