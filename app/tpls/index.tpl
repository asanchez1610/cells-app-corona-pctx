<!doctype html>

<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="format-detection" content="telephone=no">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Corona 2.0</title>
  <!-- Place favicon.ico in the `app/` directory -->

  <!-- Chrome for Android theme color -->
  <meta name="theme-color" content="#303F9F">

  <!-- Web Application Manifest -->
  <link rel="manifest" href="manifest.json">

  <!-- Tile color for Win8 -->
  <meta name="msapplication-TileColor" content="#3372DF">

  <!-- Add to homescreen for Chrome on Android -->
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="application-name" content="staticScaffold">
  <link rel="icon" sizes="192x192" href="resources/images/touch/icon-bbva192x192.png">

  <!-- Add to homescreen for Safari on iOS -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title" content="staticScaffold">
  <link rel="apple-touch-icon" href="resources/images/touch/icon-bbva144x144.png">

  <!-- Tile icon for Win8 (144x144) -->
  <meta name="msapplication-TileImage" content="resources/images/touch/ms-touch-icon-144x144-precomposed">

  <link rel="stylesheet" href="styles/main.css">
   <style>
    @import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@100;400&display=swap');
  </style>
  <script>
    window.IntlMsg = window.IntlMsg || {};
    window.IntlMsg.lang = 'es-ES';
  </script>
</head>

<body class="fullbleed layout vertical loading">
  <div id="splash"></div>
  <div id="app__content"></div>

  <!-- Header Menu -->
  <cells-header-menu non-close-session="" ></cells-header-menu>

  <cells-commons-access-control id="generic-access-control"></cells-commons-access-control>

  <cells-commons-dp id="cellsCommonsDp" genericAccessControl="#generic-access-control" ></cells-commons-dp>

  <bbva-help-modal id="msgModal" button-text="Aceptar" title-icon="coronita:info" modal-type="warning">
          <div class="content" slot="slot-content">
            <div id="textMsg">

            </div>
          </div>
  </bbva-help-modal>

  <cells-mask-loading></cells-mask-loading>

  <!-- Sript Initialize App -->
  <script src="resources/js/index.js" ></script>

  <!-- for a11y purposes -->
  <div id="announcer" aria-live="polite"></div>

  <script src="vendor/bowser.min.js"></script>
  <script type="module" src="scripts/app-module.js"></script>
</body>

</html>
