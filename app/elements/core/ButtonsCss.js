import { css } from 'lit-element';

export default css`

.button {
  position: relative;
  display: inline-block;
  vertical-align: top;
  height: 36px;
  line-height: 35px;
  padding: 0 20px;
  font-size: 13px;
  color: white;
  text-align: center;
  text-decoration: none;
  text-shadow: 0 -1px rgba(0, 0, 0, 0.4);
  background-clip: padding-box;
  cursor: pointer;
  border: 1px solid;
  outline: none;
}
.button:before {
  content: "";
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  pointer-events: none;
}
.button:hover:before {
  background-image: radial-gradient(
    farthest-corner,
    rgba(255, 255, 255, 0.18),
    rgba(255, 255, 255, 0.03)
  );
}
.button:active {
  -webkit-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.2);
}
.button:active:before {
  content: none;
}

.button-green {
  background: #5ca934;
  border-color: #478228 #478228 #3c6f22;
}
.button-green:active {
  background: #5ca934;
  border-color: #3c6f22 #478228 #478228;
}

.button-red {
  background: #d5452f;
  border-color: #ae3623 #ae3623 #992f1f;
}
.button-red:active {
  background: #d5452f;
  border-color: #992f1f #ae3623 #ae3623;
}

.button-info {
  background: #1097e6;
  border-color: #0d78b6 #0d78b6 #0b689e;
}
.button-info:active {
  background: #1097e6;
  border-color: #0b689e #0d78b6 #0d78b6;
}

.button-orange {
  background: #f4902a;
  border-color: #df770c #df770c #c76a0a;
}
.button-orange:active {
  background: #f4902a;
  border-color: #c76a0a #df770c #df770c;
}

.button-pink {
  background: #e8367f;
  border-color: #d31865 #d31865 #bc165a;
}
.button-pink:active {
  background: #e8367f;
  border-color: #bc165a #d31865 #d31865;
}

.button-black {
  background: #47494f;
  border-color: #2f3034 #2f3034 #232427;
}
.button-black:active {
  background: #47494f;
  border-color: #232427 #2f3034 #2f3034;
}

.button-blue {
  background: #1464A5;
  border-color: #2d477b #2d477b #263c68;
}
.button-blue:active {
  background: #1464A5;
  border-color: #263c68 #2d477b #2d477b;
}

.button-purple {
  background: #9966cb;
  border-color: #8040be #8040be #733aab;
}
.button-purple:active {
  background: #9966cb;
  border-color: #733aab #8040be #8040be;
}

/*  Gradients */

.button-grad-green {
  background: #5ca934;
  border-color: #478228 #478228 #3c6f22;
  background-image: -webkit-linear-gradient(top, #69c03b, #5ca934 66%, #54992f);
  background-image: -moz-linear-gradient(top, #69c03b, #5ca934 66%, #54992f);
  background-image: -o-linear-gradient(top, #69c03b, #5ca934 66%, #54992f);
  background-image: linear-gradient(to bottom, #69c03b, #5ca934 66%, #54992f);
}
.button-grad-green:active {
  background: #5ca934;
  border-color: #3c6f22 #478228 #478228;
}

.button-grad-red {
  background: #d5452f;
  border-color: #ae3623 #ae3623 #992f1f;
  background-image: -webkit-linear-gradient(top, #da5c48, #d5452f 66%, #c73d28);
  background-image: -moz-linear-gradient(top, #da5c48, #d5452f 66%, #c73d28);
  background-image: -o-linear-gradient(top, #da5c48, #d5452f 66%, #c73d28);
  background-image: linear-gradient(to bottom, #da5c48, #d5452f 66%, #c73d28);
}
.button-grad-red:active {
  background: #d5452f;
  border-color: #992f1f #ae3623 #ae3623;
}

.button-grad-info {
  background: #1097e6;
  border-color: #0d78b6 #0d78b6 #0b689e;
  background-image: -webkit-linear-gradient(top, #25a5f0, #1097e6 66%, #0f8ad3);
  background-image: -moz-linear-gradient(top, #25a5f0, #1097e6 66%, #0f8ad3);
  background-image: -o-linear-gradient(top, #25a5f0, #1097e6 66%, #0f8ad3);
  background-image: linear-gradient(to bottom, #25a5f0, #1097e6 66%, #0f8ad3);
}
.button-grad-info:active {
  background: #1097e6;
  border-color: #0b689e #0d78b6 #0d78b6;
}

.button-grad-orange {
  background: #f4902a;
  border-color: #df770c #df770c #c76a0a;
  background-image: -webkit-linear-gradient(top, #f69f47, #f4902a 66%, #f38617);
  background-image: -moz-linear-gradient(top, #f69f47, #f4902a 66%, #f38617);
  background-image: -o-linear-gradient(top, #f69f47, #f4902a 66%, #f38617);
  background-image: linear-gradient(to bottom, #f69f47, #f4902a 66%, #f38617);
}
.button-grad-orange:active {
  background: #f4902a;
  border-color: #c76a0a #df770c #df770c;
}

.button-grad-pink {
  background: #e8367f;
  border-color: #d31865 #d31865 #bc165a;
  background-image: -webkit-linear-gradient(top, #eb5190, #e8367f 66%, #e62473);
  background-image: -moz-linear-gradient(top, #eb5190, #e8367f 66%, #e62473);
  background-image: -o-linear-gradient(top, #eb5190, #e8367f 66%, #e62473);
  background-image: linear-gradient(to bottom, #eb5190, #e8367f 66%, #e62473);
}
.button-grad-pink:active {
  background: #e8367f;
  border-color: #bc165a #d31865 #d31865;
}

.button-grad-black {
  background: #47494f;
  border-color: #2f3034 #2f3034 #232427;
  background-image: -webkit-linear-gradient(top, #55585f, #47494f 66%, #3d3f44);
  background-image: -moz-linear-gradient(top, #55585f, #47494f 66%, #3d3f44);
  background-image: -o-linear-gradient(top, #55585f, #47494f 66%, #3d3f44);
  background-image: linear-gradient(to bottom, #55585f, #47494f 66%, #3d3f44);
}
.button-grad-black:active {
  background: #47494f;
  border-color: #232427 #2f3034 #2f3034;
}

.button-grad-blue {
  background: #1464A5;
  border-color: #2d477b #2d477b #263c68;
  background-image: -webkit-linear-gradient(top, #4369b6, #1464A5 66%, #365391);
  background-image: -moz-linear-gradient(top, #4369b6, #1464A5 66%, #365391);
  background-image: -o-linear-gradient(top, #4369b6, #1464A5 66%, #365391);
  background-image: linear-gradient(to bottom, #4369b6, #1464A5 66%, #365391);
}
.button-grad-blue:active {
  background: #1464A5;
  border-color: #263c68 #2d477b #2d477b;
}

.button-grad-purple {
  background: #9966cb;
  border-color: #8040be #8040be #733aab;
  background-image: -webkit-linear-gradient(top, #a87dd3, #9966cb 66%, #8f57c6);
  background-image: -moz-linear-gradient(top, #a87dd3, #9966cb 66%, #8f57c6);
  background-image: -o-linear-gradient(top, #a87dd3, #9966cb 66%, #8f57c6);
  background-image: linear-gradient(to bottom, #a87dd3, #9966cb 66%, #8f57c6);
}
.button-grad-purple:active {
  background: #9966cb;
  border-color: #733aab #8040be #8040be;
}

.button-aquadark {
  background: #028484;
  border-color: #006C6C #006C6C #733aab;
}
.button-aquadark:active {
  background: #028484;
  border-color: #028484 #028484 #028484;
}

.button-grad-aquadark {
  background: #028484;
  border-color: #006C6C #006C6C #733aab;
  background-image: -webkit-linear-gradient(top, #02A5A5, #028484 66%,#028484);
  background-image: -moz-linear-gradient(top, #02A5A5, #028484 66%, #028484);
  background-image: -o-linear-gradient(top, #02A5A5, #028484 66%, #028484);
  background-image: linear-gradient(to bottom, #02A5A5, #028484 66%, #028484);
}
.button-grad-aquadark:active {
  background: #028484;
  border-color: #02A5A5 #006C6C #028484;
}

.button-primary {
  background: #004481;
  border-color: #004481 #004481 #004481;
}
.button-primary:active {
  background: #004481;
  border-color: #004481 #004481 #004481;
}

.button-grad-primary {
  background: #1464A5;
  border-color: #1464A5;
  background-image: -webkit-linear-gradient(top, #1973B8, #004481 66%,#004481);
  background-image: -moz-linear-gradient(top, #1973B8, #004481 66%, #004481);
  background-image: -o-linear-gradient(top, #1973B8, #004481 66%, #004481);
  background-image: linear-gradient(to bottom, #1973B8, #004481 66%, #004481);
}
.button-grad-primary:active {
  background: #004481;
  border-color: #1973B8 #004481 #004481;
}

.button-white {
  color: #121212;
  background: #fff;
  border-color: #bdbdbd #bdbdbd #bdbdbd;
}
.button-white:active {
  color: #121212;
  background: #fff;
  border-color: #bdbdbd #bdbdbd #bdbdbd;
}

.button-grad-white {
  background: #fff;
  border-color: #bdbdbd;
  color: #121212;
  background-image: -webkit-linear-gradient(top, #f4f4f4, #fff 66%,#fff);
  background-image: -moz-linear-gradient(top, #f4f4f4, #fff 66%, #fff);
  background-image: -o-linear-gradient(top, #f4f4f4, #fff 66%, #fff);
  background-image: linear-gradient(to bottom, #f4f4f4, #fff 66%, #fff);
}
.button-grad-white:active {
  background: #fff;
  border-color: #bdbdbd #bdbdbd #bdbdbd;
}

.button-gray {
  color: #fff;
  background: #546e7a;
  border-color: #546e7a;
}
.button-gray:active {
  color: #fff;
  background: #546e7a;
  border-color: #546e7a;
}

.button-grad-gray {
  background: #546e7a;
  border-color: #546e7a;
  color: #fff;
  background-image: -webkit-linear-gradient(top, #455a64 , #546e7a 66%,#546e7a);
  background-image: -moz-linear-gradient(top, #455a64 , #546e7a 66%, #546e7a);
  background-image: -o-linear-gradient(top, #455a64 , #546e7a 66%, #546e7a);
  background-image: linear-gradient(to bottom, #455a64 , #546e7a 66%, #546e7a);
}
.button-grad-gray:active {
  background: #546e7a;
  border-color: #546e7a;
}

.button.lg {
  height: 46px;
  font-size: 16px;
}

.button.sm {
  height: 30px;
  font-size: 13px;
  line-height: 13px;
}

.button.full-width {
  width: 100%;
}
`;
