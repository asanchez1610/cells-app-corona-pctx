import { LitElement } from 'lit-element';

export class BaseElement extends LitElement {
  constructor() {
    super();
    this.host = this.extract(this.appProps, 'host', '');
    this.paths = this.extract(this.appProps, 'paths', {});
  }

  static get properties() {
    return {
      host: String,
      paths: Object
    };
  }

  get appProps() {
    if (window.AppConfig) {
      return window.AppConfig;
    }
    return {};
  }

  userSession(itemName) {
    if (itemName) {
      return JSON.parse(window.sessionStorage.getItem(itemName));
    }
    return JSON.parse(window.sessionStorage.getItem('userSession'));
  }

  get momentJs() {
    if (window.moment) {
      return window.moment;
    }
    return {};
  }

  extract(data, keys, value) {
    let ret;
    if (!this.isEmpty(data) && !this.isEmpty(keys)) {
      let split = keys.split('.');
      ret = data[split.shift()];
      while (ret && split.length) {
        ret = ret[split.shift()];
      }
    }
    return this.isEmpty(ret) && value !== null ? value : ret;
  }

  isEmpty(evaluate) {
    switch (typeof evaluate) {
      case 'object':
        return evaluate === null || Object.keys(evaluate).length === 0;
      case 'string':
        return evaluate === '';
      case 'undefined':
        return true;
      default:
        return false;
    }
  }

  isNotEmpty(evaluate) {
    return !this.isEmpty(evaluate);
  }

  isBlank(str) {
    return !str || /^\s*$/.test(str);
  }

  isNotBlank(str) {
    return !this.isBlank(str);
  }

  joinObjects() {
    let objJoin = {};
    if (arguments && arguments.length > 0) {
      for (let i = 0; i < arguments.length; i++) {
        const arg = arguments[i];
        objJoin = { ...objJoin, ...arg };
      }
    }
    return objJoin;
  }

  joinArrays() {
    let arrJoin = [];
    if (arguments && arguments.length > 0) {
      for (let i = 0; i < arguments.length; i++) {
        const arg = arguments[i];
        arrJoin = [...arrJoin, ...arg];
      }
    }
    return arrJoin;
  }

  dispatch(nameEvt, detail) {
    const val = typeof detail === 'undefined' ? null : detail;
    this.dispatchEvent(
      new CustomEvent(nameEvt, {
        composed: true,
        bubbles: true,
        detail: val,
      })
    );
  }

  adaptStringHtml(stringHtml) {
    var t = document.createElement('template');
    t.innerHTML = stringHtml;
    return t.content.cloneNode(true);
  }

  applyObjectParamsInUrl(path, params) {
    let urlParams = Object.keys(params)
      .map(function(k) {
        return encodeURIComponent(k) + '=' + encodeURIComponent(params[k]);
      })
      .join('&');
    return `${path}?${urlParams}`;
  }

  element(selector) {
    return this.shadowRoot.querySelector(selector);
  }

  elementsAll(selector) {
    return this.shadowRoot.querySelectorAll(selector);
  }

  getJsonFromQueryParams(path) {
    let query = path;
    let result = {};
    query.split('&').forEach(function(part) {
      let item = part.split('=');
      result[item[0]] = decodeURIComponent(item[1]);
    });
    return result;
  }

  getPositionElement(el) {
    let xPosition = 0;
    let yPosition = 0;
    while (el) {
      if (el.tagName === 'BODY') {
        const xScrollPos =
          el.scrollLeft ||
          this.shadowRoot.ownerDocument.documentElement.scrollLeft;
        const yScrollPos =
          el.scrollTop ||
          this.shadowRoot.ownerDocument.documentElement.scrollTop;
        xPosition += el.offsetLeft - xScrollPos + el.clientLeft;
        yPosition += el.offsetTop - yScrollPos + el.clientTop;
      } else {
        xPosition += el.offsetLeft - el.scrollLeft + el.clientLeft;
        yPosition += el.offsetTop - el.scrollTop + el.clientTop;
      }
      el = el.offsetParent;
    }
    return {
      x: xPosition,
      y: yPosition,
    };
  }

  appLoading(show) {
    window.dispatchEvent(
      new CustomEvent('on-global-loading', {
        detail: show,
        bubbles: true,
        composed: true,
      })
    );
  }
}
