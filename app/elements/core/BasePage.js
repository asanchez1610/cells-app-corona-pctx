import { BaseElement } from './BaseElement';
import { BbvaCoreIntlMixin } from '@bbva-web-components/bbva-core-intl-mixin';
import { CellsPage } from '@cells/cells-page';

let aggregation = (baseClass, ...mixins) => {
  let base = class _Combined extends baseClass {
    constructor(...args) {
      super(...args);
    }
  };
  let copyProps = (target, source) => {
    Object.getOwnPropertyNames(source)
      .concat(Object.getOwnPropertySymbols(source))
      .forEach((prop) => {
        if (
          prop.match(
            /^(?:constructor|prototype|arguments|caller|name|bind|call|apply|toString|length)$/
          )
        ) {
          return;
        }
        Object.defineProperty(
          target,
          prop,
          Object.getOwnPropertyDescriptor(source, prop)
        );
      });
  };
  mixins.forEach((mixin) => {
    copyProps(base.prototype, mixin.prototype);
    copyProps(base, mixin);
  });
  return base;
};

class GenericPage extends aggregation(CellsPage, BaseElement) {
  constructor() {
    super();
  }
}

const initMix = BbvaCoreIntlMixin;
export class BasePage extends initMix(GenericPage) {
  constructor() {
    super();
  }
  get dataAccess() {
    const dataAccess = localStorage.getItem('dataAccess');
    if (this.isNotEmpty(dataAccess)) {
      return JSON.parse(dataAccess);
    }
    return {};
  }
}
