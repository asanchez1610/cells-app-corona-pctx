import { BaseElement } from '../../core/BaseElement.js';

class AccountsDm extends BaseElement {
  static get properties() {
    return {};
  }

  buildQueryParams(params) {
    if (this.isEmpty(params)) {
      return '';
    }
    let arrParams = [];
    if (params['number']) {
      arrParams.push(`number_eq=${params['number']}`);      
    }
    if (params['currency']) {
      arrParams.push(`currency_eq=${params['currency']}`);      
    }
    if (params['status.id']) {
      arrParams.push(`status_eq=${params['status.id']}`);      
    }
    return `?${arrParams.join('&')}`;

  }

  async listAccounts(params) {
    this.appLoading(true);
    let requestOptions = {
      method: 'GET',
    };
    let urlSearch = `${this.host}/${this.extract(this.paths, 'accounts.list', '')}${this.buildQueryParams(params)}`;
    const result = await fetch(urlSearch, requestOptions).then((response) => response.json());
    const data = result.map(item => {
      let account = { ...item.data };
      account.id = item.id;  
      return account;
    });

    this.dispatch('list-accounts', data);
    this.appLoading(false);
  }

  async createAccount(account, accountSelected) {
    this.appLoading(true);
    let headersProps = new Headers();
    headersProps.append("Content-Type", "application/json");
    let preBody = {
      number: account.number,
      currency: account.currency,
      status: account.statusId,
      lastUpdateDate: window.moment(new Date()).format('YYYY-MM-DD HH:mm'),
      data: {
        number: account.number,
        name: account.name,
        shortName: account.shortName,
        currency: account.currency,
        swift: account.swift,
        comment: account.comment,
        lastUpdateDate: window.moment(new Date()).format('YYYY-MM-DD HH:mm'),
        status: {
          id: account.statusId
        },
        bank: {
          id: account.bic,
          location: {
            addressName: account.addressName,
            city: account.city,
            country: account.country,
            branch: {
              id: account.branchCode,
              name: account.branchName
            }
          }
        }
      }
    };
    let pathCreateAndEdit = `${this.host}/${this.extract(this.paths, 'accounts.create', '')}`;
    let method = 'POST';
    const idAccount = this.extract(accountSelected, 'id', null);
    if (idAccount) {
      method = 'PUT';
      pathCreateAndEdit = `${pathCreateAndEdit}/${idAccount}`;
      preBody.data.creationDate = window.moment(this.extract(accountSelected, 'creationDate', (new Date()))).format('YYYY-MM-DD HH:mm')
    } else {
      preBody.creationDate = window.moment(new Date()).format('YYYY-MM-DD HH:mm')
      preBody.data.creationDate = window.moment(new Date()).format('YYYY-MM-DD HH:mm')
    }
    let body = JSON.stringify(preBody);
    console.log(body);
    let requestOptions = {
      method: method,
      headers: headersProps,
      body: body
    };
    
    let request = await fetch(pathCreateAndEdit, requestOptions);
    let result = await request.json();
    let payload = JSON.parse(body);
    this.dispatch('on-complete-create-account', { request, result, payload });
    this.appLoading(false);
  }

}
customElements.define('accounts-dm', AccountsDm);
