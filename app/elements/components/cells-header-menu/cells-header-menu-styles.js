/* eslint-disable no-unused-vars */
import { css, unsafeCSS } from 'lit-element';
import * as foundations from '@bbva-web-components/bbva-foundations-styles';

export default css`:host {
  display: block;
  box-sizing: border-box;
}

:host([hidden]),
[hidden] {
  display: none !important;
}

*,
*:before,
*:after {
  box-sizing: inherit;
}

header {
  width: 100%;
  height: 62px;
  box-shadow: 0px 1px 15px 1px rgba(69, 65, 78, 0.1);
  position: fixed;
  top: 0;
  left: 0;
  z-index: 10;
  display: flex;
  align-items: center;
  justify-content: space-between;
  transition: all 0.25s ease-in-out;
}

.section-top-right {
  margin-right: 15px;
  display: flex;
  align-items: center;
}
.section-top-right img {
  width: 38px;
  height: 37px;
  border-radius: 50%;
  cursor: pointer;
  box-shadow: 0px 1px 1px 1px rgba(117, 117, 117, 0.8);
}
.section-top-right .text-info-user {
  display: flex;
  align-items: flex-end;
  flex-direction: column;
  text-shadow: 0 -1px rgba(0, 0, 0, 0.4);
  font-size: 0.75em;
  margin-right: 10px;
  font-weight: lighter;
  margin-top: 5px;
  text-transform: uppercase;
  line-height: 16px;
}
.section-top-right .text-info-user .rol {
  font-size: 0.75em;
  font-weight: normal;
}

.blue {
  background-color: #004481;
  color: #fff;
}

.blue-light {
  background-color: #1464a5;
  color: #fff;
}

.navy {
  background-color: #072146;
  color: #fff;
}

.green {
  background-color: #006c6c;
  color: #fff;
  color: #fff;
}

.light {
  background-color: #fff;
  color: #666;
}

.pink {
  background-color: #ad53a1;
  color: #fff;
}

.dark {
  background-color: #043263;
  color: #fff;
}

.section-top-left {
  margin-left: 75px;
}
.section-top-left .btn-menu-responsive {
  display: none;
  width: 75px;
  height: 62px;
  background-color: transparent;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1;
}
.section-top-left .btn-menu-responsive iron-icon {
  height: 29px;
  width: 29px;
  margin-left: 15px;
  margin-right: 15px;
  color: white;
  cursor: pointer;
  margin-top: 19px;
  height: 27px;
  width: 27px;
}
.section-top-left .logo {
  width: 240px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  color: white;
}
.section-top-left .logo img {
  width: 80px;
  cursor: pointer;
}
.section-top-left .logo h2 {
  margin: 0;
  margin-left: 10px;
  padding: 0;
  width: calc(100% - $w_log);
  font-size: 1.08em;
  font-weight: lighter;
}

.wrapper-aside {
  margin: 0;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 10;
  background-color: #2b2f3a;
  height: 100vh;
  width: 60px;
  overflow-x: hidden;
  overflow-y: auto;
  transition: width 0.15s linear;
  border-right: 1px solid rgba(69, 65, 78, 0.95);
}

.scroll-menu-vertical::-webkit-scrollbar-track {
  -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
  background-color: #F4F4F4;
}

.scroll-menu-vertical::-webkit-scrollbar {
  width: 8px;
  background-color: #F4F4F4;
}

.scroll-menu-vertical::-webkit-scrollbar-thumb {
  -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
  background-color: #555;
}

.scroll-menu-vertical::-webkit-scrollbar-thumb {
  background-color: #555;
  border-radius: 6px;
}

.collapsible {
  width: 300px;
}

aside .header-logo-sidebar {
  margin: 0;
  background-color: #0f1011;
  height: 62px;
  width: 300px;
  display: flex;
  align-items: center;
}
aside .header-logo-sidebar iron-icon {
  height: 29px;
  width: 29px;
  margin-left: 15px;
  margin-right: 15px;
  color: white;
  cursor: pointer;
}
aside .header-logo-sidebar .logo-menu {
  width: 240px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  color: white;
  margin-left: 18px;
}
aside .header-logo-sidebar .logo-menu img {
  width: 80px;
  cursor: pointer;
}
aside .header-logo-sidebar .logo-menu h2 {
  margin: 0;
  margin-left: 10px;
  padding: 0;
  width: calc(100% - $w_log);
  font-size: 1.08em;
  font-weight: lighter;
}
aside nav {
  color: white;
  padding: 0;
  margin: 0;
}
aside nav ul {
  padding: 0;
  margin: 0;
  width: 300px;
  overflow: hidden;
}
aside .active {
  color: white !important;
}
aside nav ul > li {
  list-style: none;
  display: flex;
  align-items: center;
  padding: 15px 0;
  border-bottom: 1px solid rgba(12, 12, 12, 0.35);
  font-size: 0.9em;
  color: #8991a9;
  transition: all 0.25s ease-in-out;
  cursor: pointer;
}
aside nav ul > li .icon-content {
  width: 62px;
}
aside nav ul > li .icon-content iron-icon {
  margin-left: 17px;
  height: 20px;
  width: 20px;
}
aside nav ul > li .icon-content cells-icon {
  margin-left: 20px;
  height: 20px;
  width: 20px;
}
aside nav ul > li .link-text-content {
  padding-left: 10px;
  width: calc(100% - $h_items);
}
aside nav ul > li a {
  color: inherit;
  text-decoration: none;
}
aside nav ul > li:hover {
  color: #fff;
}

.list-vertical-submenu {
  width: 100%;
  overflow: hidden;
  height: 0;
  transition: all 0.2s linear;
}
.list-vertical-submenu ul {
  margin: 0;
  padding: 0;
  width: 100%;
}
.list-vertical-submenu ul li {
  font-size: 0.8em;
  color: #8991a9;
  padding-top: 10px;
  padding-bottom: 10px;
  transition: all 0.2s ease-in-out;
}
.list-vertical-submenu ul li a {
  margin-left: 45px;
}
.list-vertical-submenu ul li a cells-icon {
  margin-right: 5px;
}
.list-vertical-submenu ul li:hover {
  color: #fff;
  text-shadow: 0 -1px rgba(0, 0, 0, 0.4);
}

.list-vertical-submenu-show {
  height: auto;
}

@media only screen and (max-width: 576px) {
  .wrapper-aside {
    width: 0;
    border-right: none;
  }

  .collapsible {
    width: 300px;
  }

  .section-top-left .btn-menu-responsive {
    display: block;
  }
}
.text-dark {
  color: #000;
}

.text-white {
  color: #fff;
}

.box-info-user-profile {
  background-color: #4d4d4d;
  box-shadow: 0 0 5px #4d4d4d;
  width: 280px;
  position: fixed;
  top: 70px;
  right: -300px;
  transition: all 0.25s ease-in-out;
  color: #fff;
  padding: 15px;
  display: flex;
  align-items: center;
  flex-direction: column;
  font-size: 0.9em;
  z-index: 20;
  text-transform: uppercase;
  padding-bottom: 20px;
}
.box-info-user-profile img {
  margin-top: 10px;
  width: 55%;
  border-radius: 50%;
  background-color: rgba(117, 117, 117, 0.8);
  box-shadow: 0px 1px 1px 1px rgba(117, 117, 117, 0.8);
}
.box-info-user-profile .name {
  margin-top: 25px;
  text-align: center;
}
.box-info-user-profile .registro {
  font-size: 0.9em;
  font-weight: bold;
  color: #5bbeff;
  margin-top: 5px;
  margin-bottom: 5px;
  cursor: pointer;
}
.box-info-user-profile .registro:hover {
  text-decoration: underline;
}
.box-info-user-profile .other-text {
  margin: 0px 0 0px 0;
  font-size: 0.85em;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 0.8em;
  text-align: center;
}
.box-info-user-profile .other-text iron-icon {
  width: 16px;
  margin-right: 5px;
}
.box-info-user-profile .btnLogOut {
  margin-top: 10px;
}

.show-box-info-user {
  right: 10px;
}

@media only screen and (max-width: 576px) {
  .section-top-right .text-info-user {
    display: none;
  }

  .section-top-left .logo {
    width: 225px;
  }
}
.horizontal aside {
  display: none;
}

.horizontal .section-top-left {
  margin-left: 15px;
}

.horizontal.wrapper-aside {
  display: none;
}

.menu-horizontal {
  width: 100%;
  height: 50px;
  box-shadow: 0px 1px 15px 1px rgba(69, 65, 78, 0.1);
  background-color: #20222a;
  position: fixed;
  top: 62px;
  left: 0px;
}
.menu-horizontal ul {
  margin: 0;
  padding: 0;
  width: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  /* Clear floats after the columns */
}
.menu-horizontal ul li {
  color: rgba(255, 255, 255, 0.5);
  font-size: 0.9em;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  padding-left: 15px;
  padding-right: 15px;
  border-right: 1px solid rgba(69, 65, 78, 0.3);
  cursor: pointer;
  transition: all 0.2s ease-in-out;
}
.menu-horizontal ul li cells-icon {
  width: 17px;
  margin-right: 8px;
  position: relative;
  top: 0px;
}
.menu-horizontal ul .dropdown-content {
  background-color: #20222a;
  display: none;
  position: absolute;
  width: max-content;
  margin: auto;
  margin-top: 0px;
  border-top: 2px solid #1973B8;
  margin-left: -15px;
  top: 50px;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  z-index: 1;
  padding-left: 10px;
  padding-right: 10px;
}
.menu-horizontal ul .column {
  float: left;
  padding: 8px 0px;
}
.menu-horizontal ul .column a {
  float: none;
  color: rgba(255, 255, 255, 0.5);
  padding: 10px 10px;
  text-decoration: none;
  display: block;
  text-align: left;
  transition: all 0.2s ease-in-out;
}
.menu-horizontal ul .column a:hover {
  color: #fff;
  text-shadow: 0 -1px rgba(0, 0, 0, 0.4);
}
.menu-horizontal ul .row:after {
  content: "";
  display: table;
  clear: both;
  width: 100%;
}
.menu-horizontal ul li:hover .dropdown-content {
  display: block;
}
.menu-horizontal ul li:hover {
  color: #fff;
  text-shadow: 0 -1px rgba(0, 0, 0, 0.4);
}

@media only screen and (max-width: 768px) {
  .horizontal .section-top-left {
    margin-left: 75px;
  }

  .horizontal aside {
    display: block;
  }

  .horizontal.wrapper-aside {
    display: flex;
  }
}
@media only screen and (max-width: 768px) {
  .menu-horizontal {
    display: none;
  }
  .menu-horizontal ul li {
    font-size: 0.73em;
  }
  .menu-horizontal ul li cells-icon {
    width: 13px;
    margin-right: 5px;
    top: -1px;
  }
}
`;