import { html } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import { BaseElement } from '../../core/BaseElement.js';
import buttonsStyles from '../../core/ButtonsCss.js';
import styles from './cells-header-menu-styles.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-icons/communication-icons';
import '@polymer/iron-icons/editor-icons';
import '@polymer/iron-icons/social-icons';
import '@polymer/iron-icons/image-icons';
import '@polymer/iron-icons/device-icons';
import '@polymer/iron-icons/av-icons';
import '@polymer/iron-icons/maps-icons';
import '@polymer/iron-icons/hardware-icons';
import '@cells-components/coronita-icons/coronita-icons.js';
import '@cells-components/cells-icon/cells-icon.js';
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-header-menu></cells-header-menu>
```

##styling-doc

@customElement cells-header-menu
*/
export class CellsHeaderMenu extends BaseElement {
  static get is() {
    return 'cells-header-menu';
  }

  // Declare properties
  static get properties() {
    return {
      titleApp: { type: String },
      defaultPage: { type: String },
      items: { type: Array },
      theme: { type: String },
      logo: { type: String },
      colorTextLogo: { type: String },
      logoMenu: { type: String },
      imgProfile: { type: String },
      defaultIconMenuOption: { type: String },
      stateCollapsed: { type: Boolean },
      user: { type: Object },
      imgFProfile: { type: String },
      imgMProfile: { type: String },
      viewMode: { type: String },
      linkHomePage: { type: String },
      nonCloseSession: { type: Boolean, attribute: 'non-close-session' }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.theme = 'dark';
    this.titleApp = 'App Name';
    this.defaultIconMenuOption = 'radio-button-checked';
    this.items = [];
    this.colorTextLogo = 'color:white';
    this.user = {};
    this.nonCloseSession = false;
    this.readyInit();
  }

  static get styles() {
    return [
      styles,
      buttonsStyles,
      getComponentSharedStyles('cells-header-menu-shared-styles'),
    ];
  }

  async readyInit() {
    await this.updateComplete;
    this.removeAllOptionsActive();
    window.addEventListener('click', () => {
      this.hideLayouts();
    });
    window.addEventListener('mouseover', () => {
      this.hideAllSubmenus();
    });
  }

  hideLayouts() {
    this.element('.wrapper-aside').classList.remove('collapsible');
    this.element('.box-info-user-profile').classList.remove(
      'show-box-info-user',
    );
    this.stateCollapsed = false;
  }

  collapsedSidebar(e) {
    this.hideAllSubmenus();
    e.stopPropagation();
    this.element('.box-info-user-profile').classList.remove(
      'show-box-info-user',
    );
    this.element('.wrapper-aside').classList.toggle('collapsible');
    if (this.element('.wrapper-aside').classList.contains('collapsible')) {
      this.stateCollapsed = true;
    } else {
      this.stateCollapsed = false;
    }
  }

  selectedOptionMenu(e, item, id, idSubMenu) {
    e.stopPropagation();
    this.dispatch('selected-option-menu', item);
    this.removeAllOptionsActive();
    this.element(`#${id}`).classList.add('active');
    this.element(`#${idSubMenu}`).classList.toggle(
      'list-vertical-submenu-show',
    );
    this.element('.box-info-user-profile').classList.remove(
      'show-box-info-user',
    );
  }

  get optionsElements() {
    return this.elementsAll('.item-option-menu') || [];
  }

  removeAllOptionsActive() {
    if (this.optionsElements) {
      this.optionsElements.forEach((item) => {
        item.classList.remove('active');
      });
    }
  }

  hideAllSubmenus() {
    const subMenus = this.elementsAll('.list-vertical-submenu');
    subMenus.forEach((subMenu) => {
      subMenu.classList.remove('list-vertical-submenu-show');
    });
  }

  hoverMenu(e, type) {
    e.stopPropagation();
    this.element('.box-info-user-profile').classList.remove(
      'show-box-info-user',
    );
    if (type === 'in') {
      this.element('.wrapper-aside').classList.add('collapsible');
      this.stateCollapsed = true;
    } else {
      this.element('.wrapper-aside').classList.remove('collapsible');
      this.stateCollapsed = false;
    }
  }

  goHome() {
    this.removeAllOptionsActive();
    if (this.linkHomePage) {
      window.location = this.linkHomePage;
    }
  }

  showInfoProfile(e) {
    e.stopPropagation();
    this.element('.box-info-user-profile').classList.toggle(
      'show-box-info-user',
    );
    this.element('.wrapper-aside').classList.remove('collapsible');
  }

  logOut() {
    this.hideLayouts();
    this.dispatch('log-out-application', {});
  }

  goProfile() {
    this.dispatch('go-profile', {});
  }

  get imageUser() {
    if (this.user && this.user.urlImgProfile) {
      return this.user.urlImgProfile;
    }
    if (this.user && this.user.sexo === 'F') {
      return this.imgFProfile;
    }
    return this.imgMProfile;
  }

  get titleLogoTpl() {
    return html` <h2 style="${this.colorTextLogo}">${this.titleApp}</h2>`;
  }

  buildSubMenuHorizontal(itemsSubMenu = [], maxRows = 6, minWidth = 'auto') {
    const itemsOptions = this.items;
    if (itemsSubMenu.length === 0 || !itemsOptions) {
      return html``;
    }
    const numColumns = Math.ceil(itemsSubMenu.length / maxRows);
    const percentaje = `${
      Math.round((100 / numColumns + Number.EPSILON) * 100) / 100
    }%`;
    const iterateColumns = [];

    for (let index = 0; index < numColumns; index += 1) {
      iterateColumns.push(index);
    }
    return html`
      <div class="dropdown-content">
        <div class="row">
          ${iterateColumns.map(
    column => html`
              <div class="column" style="width:${percentaje}">
                ${itemsSubMenu
    .filter((_, index) => {
      let end = (column + 1) * maxRows;
      if (column === iterateColumns.length) {
        end = itemsSubMenu.length - 1;
      }
      return index >= column * maxRows && index < end;
    })
    .map(
      element => html`
                      <a
                        style="min-width: ${minWidth};"
                        href="${element.page ? element.page : ''}"
                        ><cells-icon icon="coronita:forward"></cells-icon
                        >${element.name}</a
                      >
                    `,
    )}
              </div>
            `,
  )}
        </div>
      </div>
    `;
  }

  linkPageItem(page) {
    window.location.href = page ? page : '#';
  }

  // Defibne template
  render() {
    return html`
      <header
        class="${this.theme} ${this.viewMode ? this.viewMode : 'horizontal'}"
      >
        <section class="section-top-left">
          <div class="btn-menu-responsive">
            <iron-icon
              class="${this.theme === 'light' ? 'text-dark' : ''}"
              @click="${this.collapsedSidebar}"
              icon="menu"
            >
            </iron-icon>
          </div>
          <div class="logo" @mouseover="${() => this.hideAllSubmenus()}">
            <img @click="${this.goHome}" src="${this.logo}" />
            ${this.titleLogoTpl}
          </div>
        </section>
        <section class="section-top-right">
          <div class="text-info-user">
            <span
              >${this.extract(this.user, 'nombres', 'N/A')}
              ${this.extract(this.user, 'apellidos', 'N/A')}</span
            >
            <span class="rol"
              >${this.extract(this.user, 'rol.name', '--')}</span
            >
          </div>

          <img @click="${this.showInfoProfile}" src="${this.imageUser}" />
        </section>
      </header>

      <nav class="menu-horizontal" ?hidden="${this.viewMode === 'vertical'}">
        <ul>
          ${this.items.map(
    item => html`
              <li ?hidden="${item.disabled}">
                <cells-icon icon="${item.icon}"></cells-icon>
                <a href="${item.page ? item.page : '#'}">${item.name}</a>
                ${this.buildSubMenuHorizontal(
    item.items,
    item.maxRows,
    item.maxWidthColumn,
  )}
              </li>
            `,
  )}
        </ul>
      </nav>

      <div
        class="wrapper-aside scroll-menu-vertical ${this.viewMode
    ? this.viewMode
    : 'horizontal'}"
        @mouseover="${e => this.hoverMenu(e, 'in')}"
        @mouseout="${e => this.hoverMenu(e, 'out')}"
      >
        <aside>
          <section class="header-logo-sidebar">
            <iron-icon
              id="menu-icon"
              @click="${this.collapsedSidebar}"
              icon="menu"
            ></iron-icon>

            <div class="logo-menu">
              <img @click="${this.goHome}" src="${this.logoMenu}" />
              ${this.titleLogoTpl}
            </div>
          </section>
          <nav>
            <ul>
              ${this.items.map(
    (item, index) => html`
            <li ?hidden="${
  item.disabled
}" id="option-menu-${index}" class="item-option-menu"
              @click="${e => this.selectedOptionMenu(
    e,
    item,
    `option-menu-${index}`,
    `list-vertical-submenu-${index}`,
  )}">
              <div class="icon-content">
                <cells-icon icon="${
  item.icon ? item.icon : this.defaultIconMenuOption
}"></cells-icon>
              </div>
              <div class="link-text-content" @click="${()=>this.linkPageItem(item.page )}">
                ${item.name}
              </div>

            </li>

            <div class="list-vertical-submenu" id="list-vertical-submenu-${index}" >
                <ul>
                  ${(item.items || []).map(
    subItem => html`
                      <li>
                        <a href="#">
                          <cells-icon icon="coronita:forward"></cells-icon
                          >${subItem.name}</a
                        >
                      </li>
                    `,
  )}
                </ul>
              </div>
            `,
  )}
            </ul>
          </nav>
        </aside>
      </div>

      <div class="box-info-user-profile" @click="${e => e.stopPropagation()}">
        <img src="${this.imageUser}" />
        <div class="name">
          ${this.extract(this.user, 'nombres', 'N/A')}
          ${this.extract(this.user, 'apellidos', 'N/A')}
        </div>
        <div class="registro" @click="${this.goProfile}">
          ${this.extract(this.user, 'registro', '--')}
        </div>
        <div class="other-text">
          <iron-icon icon="home"></iron-icon>
          <span
            >${this.extract(this.user, 'oficina.name', '--')} -
            ${this.extract(this.user, 'oficina.code', '--')}</span
          >
        </div>
        <div class="other-text">
          <iron-icon icon="mail"></iron-icon>
          <span>${this.extract(this.user, 'email', 'No Registrado')}</span>
        </div>
        <button
          ?hidden="${this.nonCloseSession}"
          @click="${this.logOut}"
          type="button"
          class="button button-gray full-width btnLogOut"
        >
          Cerrar sesión
        </button>
      </div>
    `;
  }
}

customElements.define(CellsHeaderMenu.is, CellsHeaderMenu);