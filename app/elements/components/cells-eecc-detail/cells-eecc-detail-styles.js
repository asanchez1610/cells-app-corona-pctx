/* eslint-disable no-unused-vars */
import { css, unsafeCSS } from 'lit-element';
import * as foundations from '@bbva-web-components/bbva-foundations-styles';

export default css`:host {
  display: block;
  box-sizing: border-box;
}

:host([hidden]),
[hidden] {
  display: none !important;
}

*,
*:before,
*:after {
  box-sizing: inherit;
}

.row-detail-eecc {
  display: flex;
  color: #2B2F3A;
}
.row-detail-eecc * {
  margin-right: 5px;
  font-size: 0.9em;
  text-align: center;
  align-items: center;
}
.row-detail-eecc .header {
  background-color: #f4f4f4;
  border-bottom: 1px solid #666;
  font-size: 0.9em;
  font-weight: bold;
  margin-bottom: 5px;
  padding-top: 5px;
}
.row-detail-eecc .description {
  width: calc(100% - 260px);
  text-align: right;
  padding-right: 5px;
}
.row-detail-eecc .amount {
  width: 170px;
  text-align: right;
  padding-right: 5px;
}
.row-detail-eecc .type {
  width: 30px;
}
.row-detail-eecc .count {
  width: 60px;
  margin-right: 0px;
}
.row-detail-eecc .positive {
  color: #1973B8;
  font-weight: bold;
}
.row-detail-eecc .negative {
  color: #B12C2C;
  font-weight: bold;
}
.row-detail-eecc .symbol-operator {
  font-size: 1.2em;
}
`;