import { html } from 'lit-element';
import { BaseElement } from '../../core/BaseElement.js';
import styles from './cells-eecc-detail-styles.js';
import '../../../elements/components/cells-card-panel/cells-card-panel';
class CellsEeccDetail extends BaseElement {

  static get properties() {
    return {
        panelTitle: String,
        data: Object
     };
  }

  constructor() {
    super();
    this.panelTitle = '';
    this.data = {};
  }

  static get styles() {
    return [ styles ];
  }

  render() {
    return html`
        <cells-card-panel headerTitle="${this.panelTitle}">
            <div slot="top-bar-actions">
                <cells-icon icon="coronita:print"></cells-icon>
            </div>
            <div slot="body" >
                <div class="row-detail-eecc" >
                    <div class="description header">Their Statment al 22/05/2022</div>
                    <div class="amount header">4,123,345.00</div>
                    <div class="type header">C</div>
                    <div class="count header">Items</div>
                </div>
                <div class="row-detail-eecc" >
                    <div class="description">Their open items - <span class="positive" >Debit</span></div>
                    <div class="amount">120,123,345.00</div>
                    <div class="type positive symbol-operator">+</div>
                    <div class="count">20</div>
                </div>
                <div class="row-detail-eecc" >
                    <div class="description"><span class="negative" >Credit</span></div>
                    <div class="amount">590,123,345.00</div>
                    <div class="type negative symbol-operator">-</div>
                    <div class="count">15</div>
                </div>
                <div class="row-detail-eecc" >
                    <div class="description">Pseudo item charges - <span class="positive" >Debit</span></div>
                    <div class="amount">19,345.00</div>
                    <div class="type positive symbol-operator">+</div>
                    <div class="count">9</div>
                </div>
                <div class="row-detail-eecc" >
                    <div class="description"><span class="negative" >Credit</span></div>
                    <div class="amount">145.00</div>
                    <div class="type negative symbol-operator">-</div>
                    <div class="count">4</div>
                </div>
                <div class="row-detail-eecc" >
                    <div class="description">Pseudo item gaps - <span class="positive" >Debit</span></div>
                    <div class="amount">0.00</div>
                    <div class="type positive symbol-operator">+</div>
                    <div class="count">0</div>
                </div>
                <div class="row-detail-eecc" >
                    <div class="description"><span class="negative" >Credit</span></div>
                    <div class="amount">0.00</div>
                    <div class="type negative symbol-operator">-</div>
                    <div class="count">0</div>
                </div>
                <div class="row-detail-eecc" >
                    <div class="description">Our open items - <span class="positive" >Debit</span></div>
                    <div class="amount">570,123,345.00</div>
                    <div class="type positive symbol-operator">+</div>
                    <div class="count">15</div>
                </div>
                <div class="row-detail-eecc" >
                    <div class="description"><span class="negative" >Credit</span></div>
                    <div class="amount">142.567.00</div>
                    <div class="type negative symbol-operator">-</div>
                    <div class="count">6</div>
                </div>
                <div class="row-detail-eecc" >
                    <div class="description">Our open cheques - <span class="positive" >Debit</span></div>
                    <div class="amount">0.00</div>
                    <div class="type positive symbol-operator">+</div>
                    <div class="count">0</div>
                </div>
                <div class="row-detail-eecc" >
                    <div class="description"><span class="negative" >Credit</span></div>
                    <div class="amount">0.00</div>
                    <div class="type negative symbol-operator">-</div>
                    <div class="count">0</div>
                </div>
                <div class="row-detail-eecc" >
                    <div class="description header">Our Statment al 22/05/2022</div>
                    <div class="amount header">15,123,345.00</div>
                    <div class="type header">C</div>
                    <div class="count header">--</div>
                </div>
                <div class="row-detail-eecc" >
                    <div class="description">Diferencia</div>
                    <div class="amount">0.00</div>
                    <div class="type">C</div>
                    <div class="count">&nbsp;</div>
                </div>
                <div class="row-detail-eecc" >
                    <div class="description">Número total de items abiertos</div>
                    <div class="amount">&nbsp;</div>
                    <div class="type">&nbsp;</div>
                    <div class="count">513</div>
                </div>
                <div class="row-detail-eecc" >
                    <div class="description">Número de items inresados en linea</div>
                    <div class="amount">&nbsp;</div>
                    <div class="type">&nbsp;</div>
                    <div class="count">12</div>
                </div>
            </div>
        </cells-card-panel>
    `;
  }
}
customElements.define('cells-eecc-detail', CellsEeccDetail);
