import { html, } from 'lit-element';
import { BaseElement } from '../../core/BaseElement';
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-commons-dp></cells-commons-dp>
```

##styling-doc

@customElement cells-commons-dp
*/
export class CellsCommonsDp extends BaseElement {
  static get is() {
    return 'cells-commons-dp';
  }

  // Declare properties
   static get properties() {
    return {
      timeout: { type: Number },
      defaultMethod: { type: String },
      defaulHost: { type: String },
      eventInvoke: { type: String },
      eventLoading: { type: String },
      requests: { type: Array },
      prod: Boolean,
      genericAccessControl: { type: String }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.timeout = 30000;
    this.defaultMethod = 'GET';
    this.eventLoading = 'loading-request-dp';
    this.eventInvoke = 'send-request-dp';
    this.requests = [];
    this.genericAccessControl = 'cells-commons-access-control';
    this.addListener();
  }

  async addListener() {
    await this.updateComplete;
    window.addEventListener(this.eventInvoke || 'send-request-dp', async (e) => {
      if (e.detail) {
        this.sendRequest(e.detail)
      } else {
        console.error('Config no available', e);
      }
    });
  }

  buildUrlRequest(host, path) {
    if (path) {
      return `${host}/${path}`;
    }
    return `${host}`;
  }

  configHeaders(config) {
    let headers = {};
    if (config.contentType) {
      headers['Content-Type'] = config.contentType;
    } else {
      headers['Content-Type'] = 'application/json; charset=utf-8';
    }
    if(this.prod) {
      headers['contactId'] = window.sessionStorage.getItem('architectureContactId');
    } else{
      let contactID = window.sessionStorage.getItem('contactID');
      if(!this.isEmpty(contactID)) {
        headers['contactID'] = contactID;
      }
    }
    let tokenName = config.tokenName || 'tsec';
    headers[tokenName] = window.sessionStorage.getItem(tokenName);
    if (config.addHeaders && config.addHeaders.length > 0) {
      config.addHeaders.forEach(element => {
        headers[element.key] = element.value;
      });
    }
    return headers;
  }

  clearAllRequests() {
    if (this.requests.length > 0) {
      this.requests.forEach(controller => {
        controller.abort();
      });
    }
  }

  dispatchErrorGlobal(error) {
    console.log('errorGlobal', error);
    const eventErrorGlobal = new CustomEvent('error-request-dp', {
      detail: error,
      bubbles: true,
      composed: true
    });
    window.dispatchEvent(eventErrorGlobal);
  }

  async sendRequest(config) {
    config = config || {};
    console.log("CellsBaseDmPfco.sendRequest()", config);
    try {
      let onSuccess = config.onSuccess || function () { };
      let onError = config.onError || function () { };
      let url = this.buildUrlRequest(config.host, config.path);

      if (!config.method) {
        config.method = 'GET';
      }

      if (!config.noLoading) {
        this.dispatch(this.eventLoading, true);
      }

      if (typeof config.intentos === 'undefined') {
        config.intentos = 1;
      } else {
        config.intentos = config.intentos + 1;
      }

      let headers = this.configHeaders(config);
      let controller = new AbortController();
      this.requests.push(controller);
      let signal = controller.signal;
      let settings = {
        method: config.method,
        headers: headers,
        signal: signal,
        mode: 'cors'
      };

      if (config.body) {
        let tempBody;
        if (typeof config.body === 'object') {
          tempBody = JSON.stringify(config.body);
        } else {
          tempBody = config.body;
        }
        settings.body = tempBody;
      }

      let request = new Request(url, settings);

      fetch(request).then((response) => {
        if (response.ok) {
          if (response.status === 204 || response.status === 201) {
            if (onSuccess) {
              onSuccess({ detail: {}, request: response });
            }
          } else {
            response.json().then((result) => {
              if (onSuccess) {
                onSuccess({ detail: result, request: response });
              }
            });
          }
          this.dispatch(this.eventLoading, false);
        } else {
          if (response.status === 404) {
            let dataError = {
              status: 404,
              systemErrorCause: '404 - Servicio no disponible',
              systemErrorDescription: `No se puede acceder al url: ${url}`
            };
            if (onError) {
              onError(dataError);
            }
            if (!config.stopMsgGlobal) {
              this.dispatchErrorGlobal( dataError);
            }
          } else {
            response.json().then((error) => {
              if (onError) {
                onError(error);
              }
              if (this.isExpiredToken(error)) {
                if (config.intentos < 4) {
                  this.refreshToken(config);
                } else {
                  error = this.normalizeObjects(error);
                  if (!config.stopMsgGlobal) {
                    this.dispatchErrorGlobal( error);
                  }
                }
              } else {
                error = this.normalizeObjects(error);
                if (!config.stopMsgGlobal) {
                  this.dispatchErrorGlobal( error);
                }
              }
              this.dispatch(this.eventLoading, false);
            });
          }
        }
      }).catch((error) => {
        if (onError) {
          onError(error);
        }
        let dataError;
        if (error && (error['system-error-cause'] || error['systemErrorCause'])) {
          dataError = {
            systemErrorCause: (error['system-error-cause'] || error['systemErrorCause']),
            systemErrorDescription: (error['system-error-description'] || error['systemErrorDescription'])
          };
        } else {
          dataError = {
            systemErrorCause: 'Problemas de conectividad',
            systemErrorDescription: 'Verifique la conección de red.'
          };
        }
        this.dispatch(this.eventLoading, false);
        if (!config.stopMsgGlobal) {
          this.dispatchErrorGlobal( dataError);
        }
      });
    } catch (e) {
      console.log("Error CellsCotizaDmPfco.sendRequest", e);
      this.dispatch(this.eventLoading, false);
    }
  }

  async refreshToken(config) {
      let accessControl = document.querySelector(this.genericAccessControl);
      if (accessControl) {
        await accessControl.runEmployeeAuthentication();
        await this.sendRequest(config);
      }
    }

  isExpiredToken(error) {
    let isErrorExpiredTsec = false;
    if (error) {
      let errorCode = error['error-code'] || error.errorCode;
      const codesError = {
        tsecExpired: '68',
        tsecinvalid: '61',
        tsecNotFound: '50'
      };
      switch (errorCode) {
        case codesError.tsecExpired:
        case codesError.tsecinvalid:
        case codesError.tsecNotFound:
            isErrorExpiredTsec = true;
          break;
      }
    }
    return isErrorExpiredTsec;
  }

  normalizeObjects(obj) {
    const capitalize = (s) => {
      return s.charAt(0).toUpperCase() + s.slice(1);
    };
    const camelTransform = (text) => {
      let arr = text.split('-');
      let textcamel = '';
      arr.forEach((item, index) => {
        if (index === 0) {
          textcamel = item.toLowerCase();
        } else {
          textcamel = textcamel.concat(capitalize(item));
        }
      });
      return textcamel;
    };
    let responseTransform = {};
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        responseTransform[camelTransform(key)] = obj[key];
      }
    }
    return responseTransform;
  }


  // Define a template
  render() {
    return html`
      <slot></slot>
    `;
  }
}

customElements.define(CellsCommonsDp.is, CellsCommonsDp);
