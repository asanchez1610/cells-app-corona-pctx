import { html } from 'lit-element';
import { BaseElement } from '../../core/BaseElement.js';
import styles from './cells-data-table-styles.js';
import '@cells-components/coronita-icons/coronita-icons.js';
import '@cells-components/cells-icon/cells-icon.js';
import { loadingDataTable } from './loading.js';
import '@bbva-web-components/bbva-web-table-panel-info/bbva-web-table-panel-info.js';

class CellsDataTable extends BaseElement {
    static get is() {
        return 'cells-data-table';
      }
    
     // Declare properties
     static get properties() {
      return {
        items: Array,
        displayItems: Array,
        columnsConfig: Array,
        cls: String,
        compact: Boolean,
        modePagination: String,
        pageSize: Number,
        page: Number,
        total: Number,
        paginationData: Object,
        loading: Boolean,
        _timer: Number,
        checkBoxSelection: {
          type: Boolean,
          attribute: 'checkbox-selection',
        },
        titleNoResult: String,
        msgNoResult: String,
        showPanelPaginationTop: { type: Boolean },
        showPanelPaginationBottom: { type: Boolean }
      };
    }
    
    // Initialize properties
    constructor() {
      super();
      this.items = [];
      this.columnsConfig = [];
      this.pageSize = 10;
      this.page = 1;
      this.total = 0;
      this.paginationData = {};
      this.selectionsItems = [];
      this._timer = 0;
      this.titleNoResult = 'Sin resultados para mostrar';
      this.msgNoResult = '';
      this.showPanelPaginationBottom = true;
      this.showPanelPaginationTop = false;
      this.initDataTable();
    }
    
    async initDataTable() {
      await this.updateComplete;
      if (this.checkBoxSelection) {
        this._timer = setInterval(() => {
          this.verifyActivesInitLoad();
        }, 100);
      }
    }
    
    verifyActivesInitLoad() {
      const rows = this.elementsAll('.row-item');
      if (rows.length > 0) {
        const actives = this.elementsAll('.active');
        if (actives.length > 0) {
          this.element('#checkbox-main').setAttribute('checked', 'checked');
          this.element('#checkbox-main').checked = true;
        }
        clearInterval(this._timer);
      }
    }
    
    configDataTable() {
      if (this.modePagination === 'memory') {
        this.total = this.items.length;
        this.displayItems = this.renderMemory();
      } else if (this.modePagination === 'remote') {
        this.displayItems = this.items;
      } else {
        this.displayItems = this.items;
      }
    }
    
    renderMemory() {
      return (this.items).filter(
        (_, index) => index >= this.pageSize * (this.page - 1)
          && index < this.pageSize * this.page,
      );
    }
    
    getSelections() {
      const selections = [];
      const actives = this.elementsAll('.active');
      actives.forEach((row) => {
        const item = JSON.parse(row.dataset.item);
        selections.push(item);
      });
      return selections;
    }
    
    updated(props) {
      props.forEach((_, propName) => {
        if (propName === 'items' && this.items) {
          this.configDataTable();
        }
      });
    
      if(this.items){
        this.getSelections();
        this.verifyCheckeds();
      }
    }
    
    static get styles() {
      return [
        styles
      ]
    }

    get hideBottomBarPagination() {
      return this.items.length === 0 || !this.items; 
    }
    
    activeCheckeds() {
      const actives = this.elementsAll('.row-item');
      actives.forEach((active) => {
        if (this.checkBoxSelection) {
          if (active.querySelector('input[type=checkbox]').checked) {
            active.classList.add('active');
          }
        } else {
          active.classList.add('active');
        }
      });
    }
    
    changeChk(evt, isMain) {
      evt.stopPropagation();
      const { target } = evt;
      if (isMain) {
        const chks = this.elementsAll('.checkbox-item');
        chks.forEach((el) => {
          const item = el;
          if (target.checked) {
            item.setAttribute('checked', 'checked');
            item.checked = true;
          } else {
            item.removeAttribute('checked', 'checked');
            item.checked = false;
          }
        });
        if (target.checked) {
          this.activeAll();
        } else {
          this.removeAllActives();
        }
        this.dispatch('on-check-all', target.checked);
      } else {
        const data = JSON.parse(target.parentNode.parentNode.dataset.item);
        if (target.checked) {
          target.parentNode.parentNode.classList.add('active');
          this.dispatch('on-selected-row', data);
        } else {
          target.parentNode.parentNode.classList.remove('active');
          this.dispatch('on-deselected-row', data);
        }
        this.verifyCheckeds();
      }
    }
    
    verifyCheckeds() {
      const actives = this.elementsAll('.active');
      if (actives.length > 0) {
        this.element('#checkbox-main').setAttribute('checked', 'checked');
        this.element('#checkbox-main').checked = true;
      } else {
        this.element('#checkbox-main').removeAttribute('checked');
        this.element('#checkbox-main').checked = false;
      }
    }
    
    clearCheckBox() {
      if (this.checkBoxSelection) {
        const chks = this.elementsAll('input[type=checkbox]');
        chks.forEach((item) => {
          const el = item;
          el.removeAttribute('checked');
          el.checked = false;
        });
      }
    }
    
    checkBoxTpl(id, checked) {
      return html`<input
          .checked="${checked}"
          class="inp-cbx checkbox-item${id !== 'checkbox-main'
          ? ' chk-secundary'
          : ''}"
          id="${id}"
          type="checkbox"
          style="display: none"
          @click="${e => this.changeChk(e, id === 'checkbox-main')}"
        />
        <label class="cbx" for="${id}"
          ><span>
            <svg width="12px" height="10px" viewbox="0 0 12 10">
              <polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span
        ></label>`;
    }
    
    activeAll() {
      const actives = this.elementsAll('.row-item');
      actives.forEach((active) => {
        active.classList.add('active');
      });
    }
    
    removeAllActives() {
      const actives = this.elementsAll('.active');
      actives.forEach((active) => {
        active.classList.remove('active');
      });
    }
    
    selectedRow(item, idRow, index, e) {
      if (!e.target.classList.contains('cell')) {
        return;
      }
      let event = '';
      const isActive = this.element(`#${idRow}`).classList.contains('active');
      this.removeAllActives();
      if (!isActive) {
        this.element(`#${idRow}`).classList.add('active');
        this.element(`#col-checkbox-${index}`).setAttribute('checked', 'checked');
        this.element(`#col-checkbox-${index}`).checked = true;
        event = 'on-selected-row';
      } else {
        this.element(`#col-checkbox-${index}`).removeAttribute('checked');
        this.element(`#col-checkbox-${index}`).checked = false;
        event = 'on-deselected-row';
      }
      if (this.checkBoxSelection) {
        this.activeCheckeds();
        this.verifyCheckeds();
      }
      this.dispatch(event, item);
    }
    
    get htmlPrint() {
      return html;
    }
    
    get displayTextLeft() {
      if (this.modePagination === 'memory') {
        return html`${this.pageSize * (this.page - 1) + 1}-${this.page
          === this.totalPages
          ? this.total
          : this.pageSize * this.page}
        de ${this.total} registros`;
      }
      if (this.modePagination === 'remote') {
        return html`${this.pageSize * (this.page - 1) + 1}-${this.page
          === this.totalPages
          ? this.total
          : this.pageSize * this.page}
        de ${this.total} registros`;
      }
      return html``;
    }
    
    get totalPages() {
      try {
        return Math.ceil(this.total / this.pageSize);
      } catch (error) {
        return 1;
      }
    }
    
    navigationPage(operation) {
      if (operation === '+') {
        this.page += 1;
      } else if (operation === '-') {
        this.page -= 1;
      }
      if (this.page <= 0) {
        this.page = 1;
      }
      const { options } = this.element('#select-pages');
      for (let index = 0; index < options.length; index += 1) {
        const option = options[index];
        if (parseInt(option.value, 10) === this.page) {
          option.setAttribute('selected', 'selected');
        } else {
          option.removeAttribute('selected');
        }
      }
      this.removeAllActives();
      if (this.checkBoxSelection) {
        this.clearCheckBox();
      }
      this.requestUpdate();
      if (this.modePagination === 'memory') {
        this.displayItems = this.renderMemory();
      } else if (this.modePagination === 'remote') {
        this.reloadFromPageRemoteEvent();
      }
    }

    resetSelections() {
      this.removeAllActives();
      if (this.checkBoxSelection) {
        this.clearCheckBox();
      }
      this.requestUpdate();
    }
    
    get optionsPages() {
      const options = [];
      for (let index = 1; index <= this.totalPages; index += 1) {
        options.push(index);
      }
      return options;
    }
    
    reloadFromPageRemoteEvent() {
      this.dispatch('on-reload-from-page', {
        paginationData: this.paginationData,
        page: this.page,
      });
    }
    
    changePage(select) {
      this.page = parseInt(select.value, 10);
      if (this.modePagination === 'memory') {
        this.displayItems = this.renderMemory();
      } else if (this.modePagination === 'remote') {
        this.reloadFromPageRemoteEvent();
      }
      this.removeAllActives();
      if (this.checkBoxSelection) {
        this.clearCheckBox();
      }
    }
    
    execSort(column, orderDir) {
      const tmp = [...this.items];
      tmp.sort((a, b) => {
        let x = a[column];
        let y = b[column];
        if (typeof x === 'number' && typeof y === 'number') {
          if (orderDir === 'desc') {
            return x - y;
          }
          return y - x;
        }
        x = `${x}`.toLowerCase();
        y = `${y}`.toLowerCase();
        if (orderDir === 'desc') {
          if (x < y) {
            return -1;
          }
          if (x > y) {
            return 1;
          }
          return 0;
        }
        if (x > y) {
          return -1;
        }
        if (x < y) {
          return 1;
        }
        return 0;
      });
      this.items = tmp;
    }
    
    actualizarTabla(){
      this.requestUpdate();
    }
    
    sortTable(columnConfig, id, index) {
      if (columnConfig.sort) {
        this.removeAllActives();
        const icon = this.element(`#${id}`).querySelector('cells-icon');
        if (icon) {
          const isInit = icon.classList.contains('hide');
          let orderDir = 'desc';
          if (!isInit) {
            if (icon.icon === 'coronita:up') {
              orderDir = 'asc';
              icon.icon = 'coronita:down';
            } else {
              icon.icon = 'coronita:up';
              orderDir = 'desc';
            }
          }
          icon.classList.remove('hide');
          if (this.modePagination !== 'remote') {
            this.execSort(columnConfig.dataField, orderDir);
          }
          this.elementsAll('.icon-sort').forEach((iconSort) => {
            if (iconSort.id !== `icon-sort-${index}`) {
              iconSort.classList.add('hide');
            }
          });
          this.dispatch('on-sort-data-table', {
            sort: {
              column: columnConfig.dataField,
              orderDir,
              page: this.page,
              paginationData: this.paginationData,
            },
          });
        }
      }
    }

    paginationTpl(position) {
      let showPanel = true;
      if(position === 'top') {
        showPanel = this.showPanelPaginationTop;
      } else {
        showPanel = this.showPanelPaginationBottom;
      }

      if(!showPanel) {
        showPanel = (!this.modePagination || this.hideBottomBarPagination);
      }

      if (this.items.length === 0 || this.totalPages <= 1) {
        showPanel = false;
      }
      
      return html`
      <div class="${!showPanel ? 'hide ' : ''}panel-pagination${position === 'top' ? ' panel-pagination-top': ''}">
            <section class="panel-pagination-left">
              ${this.displayTextLeft}
            </section>
            <section class="panel-pagination-right">
              <div class="panel-pagination-right-display-select">
                <select
                  id="select-pages"
                  @change="${({ target }) => this.changePage(target)}"
                >
                  ${this.optionsPages.map(
        option => html`<option>${option}</option>`,
      )}
                </select>
                <cells-icon icon="coronita:unfold"></cells-icon>
              </div>
              <div class="panel-pagination-right-display-pages">
                de ${this.totalPages} páginas
              </div>
              <button
                @click="${() => this.navigationPage('-')}"
                ?disabled="${this.page === 1}"
              >
                <cells-icon icon="coronita:back"></cells-icon>
              </button>
              <button
                @click="${() => this.navigationPage('+')}"
                ?disabled="${this.page === this.totalPages}"
              >
                <cells-icon icon="coronita:forward"></cells-icon>
              </button>
            </section>
          </div>
      `
    }

    
    // Define a template
    render() {
      return html`
        <slot></slot>
        <div class="wrapper">
          <div class="mask-loading" ?hidden="${!this.loading}">
            ${loadingDataTable}
          </div>
          <!-- Pagination Bottom -->
          ${this.paginationTpl('top')}
          
          <div class="content-table-body" >
            <div class="table">
              <!-- Headers -->
              <div class="row header${this.cls ? ` ${this.cls}` : ''}">
                <div
                  ?hidden="${!this.checkBoxSelection}"
                  id="header-col-checkbox-main"
                  class="cell${this.compact ? ' compact' : ''} col-chk"
                >
                  ${this.checkBoxTpl('checkbox-main')}
                </div>
    
                ${this.columnsConfig.map(
        (item, index) => html`
                    <div
                      id="header-col-${index}"
                      @click="${() => this.sortTable(item, `header-col-${index}`, index)}"
                      class="cell${this.compact ? ' compact' : ''}"
                    >
                      ${item.title}
                      <cells-icon
                        id="icon-sort-${index}"
                        class="icon-sort hide"
                        icon="coronita:up"
                      ></cells-icon>
                    </div>
                  `,
      )}
              </div>
    
              <!-- Rows -->
              ${(this.displayItems || []).map(
        (record, index) => html`
                  <div
                    id="row_${index}"
                    data-item="${JSON.stringify(record)}"
                    class="row row-item${record.checked ? ' active' : ''}"
                    .style="${record.styleRowCustom ? record.styleRowCustom : ''}"
                    @dblclick="${() => this.dispatch('on-dbl-click-row', record)}"
                    @click="${e => this.selectedRow(record, `row_${index}`, index, e)}"
                  >
                    <!-- CheckBox -->
                    <div
                      ?hidden="${!this.checkBoxSelection}"
                      class="cell${this.compact ? ' compact' : ''} col-chk"
                    >
                      ${this.checkBoxTpl(`col-checkbox-${index}`, record.checked)}
                    </div>
    
                    ${this.columnsConfig.map(
          column => html`
                        <div
                          class="cell${this.compact ? ' compact' : ''}"
                          data-title="${column.title}"
                          .style="${column.columnStyle
              ? column.columnStyle(
                record[column.dataField],
                index,
                record,
              )
              : ''}"
                        >
                        ${column.columnRender
                            ? html`${
                                column.columnRender(
                                  this.extract(record,column.dataField,""),
                                  index,
                                  record,
                                  this
                                )
                              }`
                            : this.extract(record,column.dataField,"")}
                        </div>
                      `,
        )}
                  </div>
                `,
      )}
            </div>
          </div>

          <bbva-web-table-panel-info ?hidden="${!this.hideBottomBarPagination}" heading="${this.titleNoResult}">
            Es posible que no existan registros para mostrar ó no se presentan coincidencias con los filtros de busqueda informados.
          </bbva-web-table-panel-info>

          <!-- Pagination Bottom -->
          ${this.paginationTpl('bottom')}
        </div>
      `;
    }
}
customElements.define(CellsDataTable.is, CellsDataTable);