import { html } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './cells-commons-access-control-styles.js';
import { BaseElement } from '../../core/BaseElement';
import '@bbva-web-components/bbva-core-employee-authentication/bbva-core-employee-authentication';
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-commons-access-control></cells-commons-access-control>
```

##styling-doc

@customElement cells-commons-access-control
*/
export class CellsCommonsAccessControl extends BaseElement {
  static get is() {
    return 'cells-commons-access-control';
  }

  // Declare properties
  static get properties() {
    return {
      urlApplication: { type: String, attribute: 'url-aplication' },
      appId: { type: String, attribute: 'aap-id' },
      urlGrantingTicket: { type: String, attribute: 'url-granting-ticket' },
      hostGetUser: { type: String, attribute: 'host-get-user' },
      pathGetUser: { type: String, attribute: 'path-get-user' },
      authenticationMode: { type: String, attribute: 'authentication-mode' },
      userIntegrated: { type: Boolean, attribute: 'user-integrated' },
      userId: { type: String, attribute: 'user-id' },
      ivTicket: { type: String, attribute: 'iv-ticket' },
      contactID: { type: String },
      verifyTsecMinutes: { type: Number },
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.userIntegrated = false;
    this.authenticationMode = 'employee';
    this.userID = '';
    this.ivTicket = '';
    this.launchAuth();
  }

  evaluateError(message, code) {
    console.log('evaluateError', code);
    this.dispatch('error-authentication', { message, code });
  }

  async getUserBankIdm(userId, tsec) {
    let headers = new Headers();
    headers.append('tsec', tsec);
    let settings = {
      method: 'GET',
      headers: headers,
    };
    let request = await fetch(
      `${this.hostGetUser}/${this.pathGetUser}=${userId.toUpperCase()}`,
      settings
    );
    console.log('request', request);
    if (request.ok) {
      let response = await request.json();
      return response;
    } else {
      this.dispatch('error-get-user', 'Error al obtener el ausuario');
    }
    return {};
  }

  async successAuth({ detail }) {
    window.sessionStorage.setItem('tsec', detail.tsec);
    window.sessionStorage.setItem('userId', detail.userId);
    this.okAuth(detail.userId, detail.tsec);
  }

  async okAuth(registro, tsec) {
    let user = {};
    if (this.userIntegrated) {
      user = await this.getUserBankIdm(registro, tsec);
    }
    this.element('.content-loading').classList.add('hide-loading');
    setTimeout(() => {
      this.element('.content-loading').classList.add('hide');
    }, 1510);
    this.dispatch('app-autenthication-success', user);
  }

  ticketError({ detail }) {
    this.evaluateError(
      `Ha ocurrido un conflicto en servicio de Granting Ticket: ${this.urlGrantingTicket}`,
      detail
    );
  }

  idpError({ detail }) {
    console.log('idpError', detail);
    //window.location = this.urlApplication;
  }

  idpAssertionError({ detail }) {
    this.evaluateError('Ha ocurrido un error de IDP Assertion.', detail);
    window.location = this.urlApplication;
  }

  ssoError({ detail }) {
    this.evaluateError('Ha ocurrido un error de SSO.', detail);
  }

  async generateGT() {
    try {
      const bodyGT = {
        authentication: {
          userID: this.userId,
          consumerID: this.appId,
          authenticationType: '00',
          authenticationData: [
            {
              idAuthenticationData: 'iv_ticketService',
              authenticationData: [this.ivTicket],
            },
          ],
        },
        backendUserRequest: {
          userId: '',
          accessCode: '',
          dialogId: '',
        },
      };
      console.log('bodyGT', bodyGT);
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      let settings = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(bodyGT),
      };
      let request = await fetch(`${this.urlGrantingTicket}`, settings);

      if (request.ok) {
        window.sessionStorage.setItem('tsec', request.headers.get('tsec'));
        window.sessionStorage.setItem('userId', this.userId);
        this.okAuth(this.userId, request.headers.get('tsec'));
      }
    } catch (error) {
      this.element('.content-loading').classList.add('hide-loading');
      setTimeout(() => {
        this.element('.content-loading').classList.add('hide');
      }, 1510);
      this.dispatch('error-authentication', {
        message: 'Error de autenticación.',
        code: '00',
      });
    }
  }

  async launchAuth() {
    await this.updateComplete;
    if (this.authenticationMode === 'employee') {
      this.element('#employeeAuth').init();
    } else {
      await this.generateGT();
    }
  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('cells-commons-access-control-shared-styles'),
    ];
  }

  async runEmployeeAuthentication() {
    if (this.authenticationMode === 'employee') {
      let employeeAuthentication = this.shadowRoot.querySelector(
        'bbva-core-employee-authentication'
      );
      await employeeAuthentication.init();
    } else {
      await this.generateGT();
    }
  }

  // Define a template
  render() {
    return html` <div class="content-loading">
        <div class="loadingio-spinner-ripple-8fpxb54ybkl">
          <div class="ldio-2hgp9rk3wnk">
            <div></div>
            <div></div>
          </div>
        </div>
      </div>

      <bbva-core-employee-authentication
        id="employeeAuth"
        granting-ticket-url="${this.urlGrantingTicket}"
        aap-id="${this.appId}"
        @employee-authentication-success="${this.successAuth}"
        @granting-ticket-error="${this.ticketError}"
        @idp-error="${this.idpError}"
        @idp-assertion-error="${this.idpAssertionError}"
        @sso-error="${this.ssoError}"
      ></bbva-core-employee-authentication>`;
  }
}

customElements.define(CellsCommonsAccessControl.is, CellsCommonsAccessControl);
