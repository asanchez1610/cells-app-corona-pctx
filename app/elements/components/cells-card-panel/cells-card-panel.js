import { html } from 'lit-element';
import { BaseElement } from '../../core/BaseElement.js';
import styles from './cells-card-panel-styles.js';
import '@cells-components/coronita-icons/coronita-icons.js';
import '@cells-components/cells-icon/cells-icon.js';

export class CellsCardPanel extends BaseElement {
  static get is() {
    return 'cells-card-panel';
  }

  // Declare properties
  static get properties() {
    return {
      headerTitle: String,
      footer: {
        type: Boolean,
        attribute: 'footer',
      },
      collapsible: {
        type: Boolean,
        attribute: 'collapsible',
      },
      showActionButton: {
        type: Boolean,
        attribute: 'show-action-button',
      },
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.headerTitle = '';
  }

  static get styles() {
    return [ styles ];
  }

  isExpand() {
    return !this.element('.body-card').classList.contains('collapsed-card');
  }

  collapsedPanel() {
    if (!this.collapsible) {
      return;
    }
    this.element('.body-card').classList.toggle('collapsed-card');
    this.dispatch('on-collapsed-panel', {
      expand: !this.element('.body-card').classList.contains('collapsed-card'),
    });
  }

  actionButtonHandler(e) {
    e.stopPropagation();
    const { clientX, clientY } = e;
    const position = this.getPositionElement(e.target);
    this.dispatch('on-handler-action-button', { clientX, clientY, position });
  }

  // Define a template
  render() {
    return html`
      <main class="card shadow">
        <header @click="${this.collapsedPanel}" ?hidden="${!this.headerTitle}">
          <section class="title">
            <div>${this.headerTitle}</div>
          </section>
          <section class="actions">
            <slot name="top-bar-actions"></slot>
            <button
              @click="${this.actionButtonHandler}"
              ?hidden="${!this.showActionButton}"
            >
              <cells-icon icon="coronita:configuration"></cells-icon>
            </button>
          </section>
        </header>

        <section class="body-card">
          <div><slot name="toolbar-top"></slot></div>
          <div class="body-content">
            <slot name="body"></slot>
          </div>
        </section>
        <footer ?hidden="${!this.footer}">
          <slot name="footer"></slot>
        </footer>
      </main>
    `;
  }
}

customElements.define(CellsCardPanel.is, CellsCardPanel);
