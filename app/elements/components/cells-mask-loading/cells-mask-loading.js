import { html } from 'lit-element';
import { BaseElement } from '../../core/BaseElement.js';
import styles from './cells-mask-loading-styles.js';
import { loading } from './loading.js';

class CellsMaskLoading extends BaseElement {
    // Declare properties
  static get properties() {
    return {
      show: { type: Boolean, attribute: 'show' }
    };
  }

  // Initialize properties
  constructor() {
    super();
  }

  static get styles() {
    return [
      styles
    ];
  }

  // Define a template
  render() {
    return html`
      <slot></slot>
      <div ?hidden="${!this.show}" class="container-loading">
        ${loading}
      </div>
    `;
  }
}
customElements.define('cells-mask-loading', CellsMaskLoading);