import { html } from 'lit-element';
import '@cells-components/cells-template-paper-drawer-panel';
import { BasePage } from '../core/BasePage.js';
import stylesPages from '../../elements/utils/styles-pages';
import gridStyles from '../core/GridSystemCss';
import buttonsStyles from '../core/ButtonsCss';

export class MasterPage extends BasePage {

  static get properties() {
    return { appProps: Object };
  }

  constructor() {
    super();
    this.appProps = window.AppConfig;
  }

  messageToast({show, message, type, icon, duration}) {
    let toast = this.element('bbva-notification-toast');
    toast.show = show;
    toast.innerHTML = `<p>${message}</p>`;
    toast.type = type;
    toast.iconExplainer = icon;
    if (duration) {
      toast.duration = duration;
    }
  }

  closeToastPage() {
    this.element('bbva-notification-toast').show = false;
  }

  content(contentHtml) {
    return html` <cells-template-paper-drawer-panel mode="seamed">
      <div slot="app__main" class="content">
        <div class="page-content">
          <main class="main-page">${contentHtml}</main>
        </div>
        <div class="container-toast">
        <bbva-notification-toast 
            @click="${this.closeToastPage}"
            fixed-bottom="" auto-hide duration="4000" animation-closed="" >
            
        </bbva-notification-toast>
        </div>
      </div>
    </cells-template-paper-drawer-panel>`;
  }
}

export { stylesPages, buttonsStyles, gridStyles };
