import { css } from 'lit-element';
export default css`

    * {
        font-family: 'Montserrat', sans-serif;
        font-size: 16px;
     }

    .content {
        margin: 0px;
            padding-top: 78px;
            padding-bottom: 20px;
            background-color: #f4f4f4;
    }
    .main-page {
        box-shadow: 0 2px 2px rgb(0, 0, 0, 0.3 );
        width: calc(100% - 40px);
        padding: 15px 20px;
        background-color: white;
        border: 1px solid #e1e1e1;
        min-height: calc(100vh - 130px);
    }
    .page-content {
        margin: 0px 20px 0px 80px;
    }

    h2 {
        margin: 0;
        font-weight: normal;
        margin-top: 5px;
        margin-bottom: 10px;
    }

    .title-page {
        font-size: 1.3em;
        padding-bottom: 10px;
        border-bottom: 1px solid #e1e1e1;
    }

    #wrapper {

}

.col-section {
    margin-bottom: 10px;
}

/* CSS Simple Pre Code */
pre {
    background: #333;
    white-space: pre;
    word-wrap: break-word;
    overflow: auto;
}

pre.code {
    margin: 0;
    border-radius: 4px;
    border: 1px solid #292929;
    position: relative;
}

pre.code label {
    font-family: sans-serif;
    font-weight: bold;
    font-size: 13px;
    color: #ddd;
    position: absolute;
    left: 1px;
    top: 15px;
    text-align: center;
    width: 60px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    pointer-events: none;
}

pre.code code {
    font-family: "Inconsolata","Monaco","Consolas","Andale Mono","Bitstream Vera Sans Mono","Courier New",Courier,monospace;
    display: block;
    margin: 0 0 0 60px;
    padding: 15px 15px 16px 14px;
    border-left: 1px solid #555;
    overflow-x: auto;
    font-size: 13px;
    line-height: 19px;
    color: #ddd;
}

.pre::after {
    content: "double click to selection";
    padding: 0;
    width: auto;
    height: auto;
    position: absolute;
    right: 18px;
    top: 14px;
    font-size: 12px;
    color: #ddd;
    line-height: 20px;
    overflow: hidden;
    -webkit-backface-visibility: hidden;
    transition: all 0.3s ease;
}

pre:hover::after {
    opacity: 0;
    visibility: visible;
}

pre.code-css code {
    color: #91a7ff;
}

pre.code-html code {
    color: #aed581;
}

pre.code-javascript code {
    color: #ffa726;
}

pre.code-json code {
    color: #B6A8EE;
}

pre.code-jquery code {
    color: #4dd0e1;

}

.btn-full-width {
    width: 100%;
}

@media only screen and (max-width: 576px)  {
    .page-content {
        margin: 0px 20px 0px 20px;
    }
}

.clone {
    padding: 5px 0;
}

.container-toast {
    position: relative;
    margin-left: 60px;
}

`;
