const columsnAccount = [
  {
    title: 'Número de cuenta',
    dataField: 'number',
    columnRender: (value, index, row) => {
      return value;
    },
  },
  {
    title: 'Moneda',
    dataField: 'currency',
    columnRender: (value, index, row) => {
      return value;
    },
  },
  {
    title: 'Fecha de creación',
    dataField: 'creationDate',
    columnRender: (value, index, row) => {
      return window.moment(value).format('DD/MM/YYYY HH:mm');
    },
  },
  {
    title: 'Estado',
    dataField: 'status',
    columnRender: (value, index, row, parentElement) => {
      if (value.id === 'ACTIVE') {
        return parentElement.htmlPrint`<b style="color:#1464A5;">Activo</b>`;
      }
      return parentElement.htmlPrint`<b style="color:#C0475E;">Inactivo</b>`;
    },
  }
];

export { columsnAccount };
