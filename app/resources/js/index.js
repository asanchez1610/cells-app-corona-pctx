const dataStorage= ()=>{
  let dataAccess = localStorage.getItem('dataAccess');
  if(dataAccess) {
    return JSON.parse(dataAccess);
  }
  return null;
}

document.addEventListener("DOMContentLoaded", async() => {
    let confs = window.AppConfig;
    const dataAccess = dataStorage();
    let newDataAccess = {};
    const existDataAccess = !!dataAccess;
   
    let urlGrantingTicket = confs.securityConf.urlGrantingTicket;
    if(existDataAccess && dataAccess.urlGrantingTicket) {
      urlGrantingTicket = dataAccess.urlGrantingTicket;
    }
    newDataAccess.urlGrantingTicket = urlGrantingTicket;
    
    let urlApplication = confs.securityConf.urlApplication;
    if(existDataAccess && dataAccess.urlApplication) {
      urlApplication = dataAccess.urlApplication;
    }
    newDataAccess.urlApplication = urlApplication;

    let aapId = confs.securityConf.aapId;
    if(existDataAccess && dataAccess.aapId) {
      aapId = dataAccess.aapId;
    }
    newDataAccess.aapId = aapId;

    let ivUser = confs.securityConf.ivUser;
    if(existDataAccess && dataAccess.ivUser) {
      ivUser = dataAccess.ivUser;
    }
    newDataAccess.ivUser = ivUser;
    
    let ivTicket = confs.securityConf.ivTicket;
    if(existDataAccess && dataAccess.ivTicket) {
      ivTicket = dataAccess.ivTicket;
    }
    newDataAccess.ivTicket = ivTicket;

    let host = confs.services.host;
    if(existDataAccess && dataAccess.host) {
      host = dataAccess.host;
    }
    newDataAccess.host = host;
    
    let accessControl = document.querySelector("cells-commons-access-control");
    accessControl.setAttribute("url-granting-ticket", urlGrantingTicket);
    accessControl.setAttribute("url-aplication", urlApplication);
    accessControl.setAttribute("aap-id", aapId);
    //Autenticacion con generacion TSEC IvUSer - IvTicket
    //accessControl.setAttribute("authentication-mode", 'generateGT');
    accessControl.setAttribute("user-id", ivUser);
    accessControl.setAttribute("iv-ticket", ivTicket);
    accessControl.userIntegrated = false;
    localStorage.setItem('dataAccess', JSON.stringify(newDataAccess));

    let headerMenu = document.querySelector("cells-header-menu");
    headerMenu.imgFProfile = confs.imgFProfile;
    headerMenu.imgMProfile = confs.imgMProfile;
    headerMenu.titleApp = confs.titleApp;
    headerMenu.logo = confs.logo;
    headerMenu.logoMenu = confs.logoMenu;
    headerMenu.viewMode = confs.viewMode;
    const options = confs.options_menu;
    document.querySelector('cells-header-menu').items = options;
    let user = {"id":"5fc9400b3540d27ff226e289","email":"asanchez.sys@gmail.com","registro":"P027140","nombres":"Jose Arturo","apellidos":"Sanchez Soto","oficina":{"name":"Miraflores","code":"0333"},"sexo":"M","rol":{"name":"Administrador","code":"admin"},"token":""};
    headerMenu.user = user;
    
  
    accessControl.addEventListener("app-autenthication-success", async({ detail }) => {
      console.log('app-autenthication-success', detail);
      /*if (confs.authenticationMode === 'employee') {
        user = {
            id: detail.data[0].id,
            email: detail.data[0].employee.email,
            registro: detail.data[0].employee.id,
            nombres: detail.data[0].firstName,
            apellidos: detail.data[0].fullLastName,
            oficina: { name: detail.data[0].budgetCenter.description, code: detail.data[0].budgetCenter.number },
            sexo: detail.data[0].employee.gender.substr(0, 1),
            rol: { name: "Administrador", code: "admin" }
          };
        headerMenu.user = user;
      }*/
    });
  
    window.addEventListener('error-request-dp', ({detail})=> {
      console.log('error-request-dp', detail);
      document.querySelector('#textMsg').innerHTML = `${detail.errorMessage ? detail.errorMessage : detail.systemErrorDescription ? detail.systemErrorDescription : 'Ha ocurrido un error inesperado.'}`;
      document.querySelector('#msgModal').open();
    });
  
    window.addEventListener('error-authentication', ({detail})=> {
      console.log('error-authentication', detail);
      document.querySelector('#textMsg').innerHTML = `${detail.message}`;
      document.querySelector('#msgModal').open();
    });
  
    window.addEventListener('on-generate-tsec', async({detail})=> {
      console.log('on-generate-tsec', detail);
      accessControl.setAttribute("url-granting-ticket", detail.urlGrantingTicket);
      accessControl.setAttribute("user-id", detail.ivUser);
      accessControl.setAttribute("iv-ticket", detail.ivTicket);
      accessControl.setAttribute("aap-id", detail.aapId);
      await accessControl.runEmployeeAuthentication();
      let dataAccess = dataStorage();
      if(!dataAccess) {
        dataAccess = {};
      }
      dataAccess.urlGrantingTicket = detail.urlGrantingTicket;
      dataAccess.ivUser = detail.ivUser;
      dataAccess.ivTicket = detail.ivTicket;
      dataAccess.aapId = detail.aapId;
      dataAccess.host = detail.host;
    });

    window.addEventListener('on-global-loading', async({detail})=> {
      document.querySelector('cells-mask-loading').show = detail;
    });
  
  });